gen = src/generated

.PHONY: build
build:
	@if [ -d $(gen) ]; then rm -r $(gen); fi; \
	mkdir $(gen); \
	cd grammar; \
	java -jar ../lib/antlr-4.7-complete.jar -Dlanguage=Python3 Cpp.g4 -visitor -o ../$(gen); \
	java -jar ../lib/antlr-4.7-complete.jar -Dlanguage=Python3 Java.g4 -visitor -o ../$(gen)

.PHONY: gui
gui: build
	@FLASK_APP=src/tcoreweb.py flask run

.PHONY: vis
vis: build
	@dot -Tpdf AST.dot -o AST.pdf; \
	dot -Tpdf TBT.dot -o TBT.pdf

.PHONY: clean
clean:
	rm -rfv $(gen) AST.xml AST.dot AST.pdf TBT.xml TBT.dot TBT.pdf *.log web/uploads/*.java

.PHONY: test
test: build
	@./test.sh
