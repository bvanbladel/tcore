
int main(){
    int x = 5;
    int y = 10;
    Rectangle r = Rectangle();
    r.setWidth(x);
    r.setHeight(y);
    assertEquals(x, r.getWidth());
    assertEquals(y, r.getHeight());
    return 0;
}
