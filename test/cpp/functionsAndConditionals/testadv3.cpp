
int setupData(int a, int b){
    if (a > b) {
        return 1;
    } else {
        return 0;
    }
}

int setupWidth(int x, int y) {
    if (x != 5) {
        if (y == 5){
            return y;
        }
        return 5;
    } else {
        return x;
    }
}

int setupHeight(int x, int y) {
    if (y != 10) {
        if (x == 10){
            return x;
        }
        return 10;
    } else {
        return y;
    }
}
int main(){
    int testWidth = 10;
    int testHeight = 5;

    if (setupData(testWidth, testHeight)){
        testWidth = setupWidth(testWidth, testHeight);
        testHeight = setupHeight(testWidth, testHeight);
    }

    Rectangle testRectangle = Rectangle();
    testRectangle.setWidth(testWidth);
    testRectangle.setHeight(testHeight);
    assertEquals(testWidth, testRectangle.getWidth());
    assertEquals(testHeight, testRectangle.getHeight());
    return 0;
}
