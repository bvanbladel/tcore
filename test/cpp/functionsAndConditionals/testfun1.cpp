
int setupWidth(){
    return 5;
}

int setupHeight(){
    return 10;
}

int main(){
    int testWidth = setupWidth();
    int testHeight = setupHeight();
    Rectangle testRectangle = Rectangle();
    testRectangle.setWidth(testWidth);
    testRectangle.setHeight(testHeight);
    assertEquals(testWidth, testRectangle.getWidth());
    assertEquals(testHeight, testRectangle.getHeight());
    return 0;
}
