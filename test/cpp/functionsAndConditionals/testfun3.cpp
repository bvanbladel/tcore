
int setupWidth(int x){
    int temp = 2;
    return x/temp;
    return 0;
}

int setupHeight(int y){
    int temp = 2;
    return y*temp;
    return 0;
}

int main(){
    int testWidth = setupWidth(10);
    int testHeight = setupHeight(5);
    Rectangle testRectangle = Rectangle();
    testRectangle.setWidth(testWidth);
    testRectangle.setHeight(testHeight);
    assertEquals(testWidth, testRectangle.getWidth());
    assertEquals(testHeight, testRectangle.getHeight());
    return 0;
}
