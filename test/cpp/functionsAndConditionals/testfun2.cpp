int setupWidth(int x){
    return x/2;
}

int setupHeight(int y){
    return y*2;
}

int main(){
    int testWidth = setupWidth(10);
    int testHeight = setupHeight(5);
    Rectangle testRectangle = Rectangle();
    testRectangle.setWidth(testWidth);
    testRectangle.setHeight(testHeight);
    assertEquals(testWidth, testRectangle.getWidth());
    assertEquals(testHeight, testRectangle.getHeight());
    return 0;
}
