int main(){
    Rectangle testRectangle = Rectangle();
    int testWidth = 5;
    testRectangle.setWidth(testWidth);
    assertEquals(testWidth, testRectangle.getWidth());
    
    int testHeight = 10;
    testRectangle.setHeight(testHeight);
    assertEquals(testHeight, testRectangle.getHeight());
    return 0;
}
