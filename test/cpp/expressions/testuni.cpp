
int main(){
    int testWidth = 4;
    int testHeight = testWidth + 7;
    ++testWidth;
    testHeight--;
    Rectangle testRectangle = Rectangle();
    testRectangle.setWidth(testWidth);
    testRectangle.setHeight(testHeight);
    assertEquals(testWidth, testRectangle.getWidth());
    assertEquals(testHeight, testRectangle.getHeight());
    return 0;
}
