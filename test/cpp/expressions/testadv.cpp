
int main(){
    int testWidth = 1;
    int testHeight = ((++testWidth) * 2) + ((testWidth++) * 3);
    testWidth = testWidth + 2;
    testWidth = testWidth++;
    Rectangle testRectangle = Rectangle();
    testRectangle.setWidth(testWidth);
    testRectangle.setHeight(testHeight);
    assertEquals(testWidth, testRectangle.getWidth());
    assertEquals(testHeight, testRectangle.getHeight());
    return 0;
}
