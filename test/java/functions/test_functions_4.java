public class Main {

    public int setupData(int i){
        if (i == 1){
            return 5;
        } else {
            if (i == 2) {
                return 5 + 5;
            }
        }
        return 0;
    }


	@Test
	public void main() {
        int testWidth = setupData(1);
        int testHeight = setupData(2);
        Rectangle testRectangle = Rectangle();
        testRectangle.setWidth(testWidth);
        testRectangle.setHeight(testHeight);
        assertEquals(testWidth, testRectangle.getWidth());
        assertEquals(testHeight, testRectangle.getHeight());
	}
}
