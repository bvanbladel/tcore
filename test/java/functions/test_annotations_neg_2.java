
public class Main {

    int testWidth;
    int testHeight;

    Rectangle r;

    public void setUp() {
        testWidth = 5;
        testHeight = 10;
        r = new Rectangle();
    }

    @After
    public void tearDown() {
    }

    @Test
	public void main() {
		r.setWidth(testWidth);
		r.setHeight(testHeight);
		assertEquals(testWidth, r.getWidth());
		assertEquals(testHeight, r.getHeight());
	}
}
