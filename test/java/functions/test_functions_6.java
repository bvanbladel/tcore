public class Main {

    public int setupData(int[] array){
        MyList<Integer> list = new MyList<Integer>();
        for (int i = 0; i < array.length; ++i) {
            list.insert(new Integer(array[i]));
        }
        return list;
    }

    @Test
	public void main() {
        int[] array = { 1, 2, 3, 4, 5};
        MyList<Integer> list = setupData(array);
        assertEquals(array.length, list.size());
    }
}
