public class Main {

    @Test
	public void main() {
        int testWidth = setupWidthBlackBox(10);
        int testHeight = setupHeightBlackBox(5);
        Rectangle testRectangle = Rectangle();
        testRectangle.setWidth(testWidth);
        testRectangle.setHeight(testHeight);
        assertEquals(testWidth, testRectangle.getWidth());
        assertEquals(testHeight, testRectangle.getHeight());
    }
}
