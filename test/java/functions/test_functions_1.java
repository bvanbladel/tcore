public class Main {

    public static int setupWidth(){
        return 5;
    }

    public static int setupHeight(){
        return 10;
    }

	@Test
	public void main() {
        int testWidth = setupWidth();
        int testHeight = setupHeight();
        Rectangle testRectangle = Rectangle();
        testRectangle.setWidth(testWidth);
        testRectangle.setHeight(testHeight);
        assertEquals(testWidth, testRectangle.getWidth());
        assertEquals(testHeight, testRectangle.getHeight());
    }
}
