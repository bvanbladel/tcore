
public class Main {

	@Test
	public void main() {
		int[] testArray = {1,2,3};
		MyVector v = new MyVector(1,2,3);

        for (int i = 0; i < 3; i++){
	        assertEquals(testArray[i], v.getElem(i));
        }
	}
}
