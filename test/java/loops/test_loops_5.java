
public class Main {

	@Test
	public void main() {
		int[] testArray = {1,2,3};
		MyVector v = new MyVector(1,2,3);

        for (int i = 0; i < testArray.length; i++){
	        assertEquals(testArray[i], v.getElem(i));
        }
	}
}
