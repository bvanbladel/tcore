
public class Main {

	@Test
	public void main() {
		int[] testArray = {1,2,3};
		MyVector v = new MyVector(1,2,3);
        int i = 0;
        for (int testElement : testArray){
	        assertEquals(testElement, v.getElem(i));
            i++;
        }
	}
}
