
public class Main {

	@Test
	public void main() {
		int[] testArray = {1,2,3};
		MyVector v = new MyVector(1,2,3);
        int i = 0;
        while ( i < 3 ){
	        assertEquals(testArray[i], v.getElem(i));
            i++;
        }
	}
}
