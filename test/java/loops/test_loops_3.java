
public class Main {

	@Test
	public void main() {
		int[] testArray = {1,2,3};
		MyVector v = new MyVector(1,2,3);
        int i = 0;
        while ( v.hasNext() ){
	        assertEquals(testArray[i], v.getNext());
            i++;
        }
	}
}
