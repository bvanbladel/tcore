
public class Main extends TestCase {

	public void testMain() {
		int testWidth = 5;
		int testHeight = 10;
		Rectangle r = new Rectangle();
		r.setWidth(testWidth);
		r.setHeight(testHeight);
		assertEquals(testWidth, r.getWidth());
		assertEquals(testHeight, r.getHeight());
	}
}
