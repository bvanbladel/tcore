public class Main {

    @Test
	public void main() {
        int testWidth = 0;
        int testHeight = 0;

        if (1 + 1 == 2){
            testHeight = 10;
        }

        if (testHeight != 10){
            testWidth = 7;
        } else {
            testWidth = 5;
        }

        Rectangle testRectangle = Rectangle();
        testRectangle.setWidth(testWidth);
        testRectangle.setHeight(testHeight);
        assertEquals(testWidth, testRectangle.getWidth());
        assertEquals(testHeight, testRectangle.getHeight());
    }
}
