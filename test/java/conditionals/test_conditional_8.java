public class Main {

    @Test
	public void main() {
        int testWidth = 0;
        int testHeight = 0;

        if (returnTrue()){
            testWidth = 5;
            testHeight = 10;
        } else {
            testWidth = 1;
            testHeight = 2;
            testWidth = 7;
            testHeight = 8;
        }

        Rectangle testRectangle = Rectangle();
        testRectangle.setWidth(testWidth);
        testRectangle.setHeight(testHeight);
        assertEquals(testWidth, testRectangle.getWidth());
        assertEquals(testHeight, testRectangle.getHeight());
    }
}
