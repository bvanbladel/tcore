public class Main {

    boolean setupData(){
        return false;
    }

    @Test
	public void main() {
        int testWidth = 0;
        int testHeight = 0;

        if (!setupData()){
            testHeight = 10;
            testWidth = 5;
        }

        Rectangle testRectangle = Rectangle();
        testRectangle.setWidth(testWidth);
        testRectangle.setHeight(testHeight);
        assertEquals(testWidth, testRectangle.getWidth());
        assertEquals(testHeight, testRectangle.getHeight());
    }
}
