public class Main {

    @Test
	public void main() {
        int testWidth = 0;
        int testHeight = 0;

        if (returnTrue()){
            testWidth = 5;
            testHeight = 10;
        } else {
            testWidth = 1;
            testHeight = 2;
            if (false || returnFalse()) {
                testWidth = 3;
                testHeight = 4;
            } else {
                testWidth = 5;
                testHeight = 6;
            }
            testWidth = 7;
            testHeight = 8;
        }
        testWidth = 5;
        testHeight = 10;

        Rectangle testRectangle = Rectangle();
        testRectangle.setWidth(testWidth);
        testRectangle.setHeight(testHeight);
        assertEquals(testWidth, testRectangle.getWidth());
        assertEquals(testHeight, testRectangle.getHeight());
    }
}
