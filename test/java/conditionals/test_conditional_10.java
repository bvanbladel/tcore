public class Main {

    @Test
	public void main() {
        int testWidth = 10;
        int testHeight = 5;

        Rectangle testRectangle = Rectangle();
        testRectangle.setWidth(testWidth);
        testRectangle.setHeight(testHeight);

        if (testRectangle != null){
            if (checkCondition()){
                assertEquals(testWidth, testRectangle.getWidth());
                assertEquals(testHeight, testRectangle.getHeight());
            }
        }
    }
}
