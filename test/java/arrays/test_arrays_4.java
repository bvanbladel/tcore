
public class Main {

	@Test
	public void main() {
		int[] testArray = {1,2,3};
        testArray[0]++;
        testArray[1] = 3;
        testArray[2] = testArray[0] + testArray[1];
		assertEquals(testArray[0], 2);
        assertEquals(testArray[1], 3);
		assertEquals(testArray[2], 5);
	}
}
