
public class Main {

	@Test
	public void main() {
		int[] testArray = getArray();
		MyVector v = new MyVector(1,2,3);
		assertEquals(testArray[0], v.getElem(0));
        assertEquals(testArray[1], v.getElem(1));
		assertEquals(testArray[2], v.getElem(2));
	}
}
