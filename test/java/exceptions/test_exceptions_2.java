
public class Main {

    @Test
    public void main() {
        try {
            Rectangle r = new Rectangle();
            r.setWidth(5);
            r.setHeight(10);
            fail("Exception not thrown");
        } catch (Throwable e) {
            assertEquals("Out of Range", e.getMessage());
        }
    }
}
