
public class Main {

	@Test
    public void main() {
		int x = 5;
		int y = 10;
		Rectangle r = new Rectangle();
		r.setWidth(x);
		r.setHeight(y);
		assertEquals(y, r.getWidth());
		assertEquals(x, r.getHeight());
	}
}
