
public class Main {

    @Test
	public void main() {
		Rectangle r = new Rectangle();
		r.setWidth(10);
		r.setHeight(5);
		assertEquals(10, r.getWidth());
		assertEquals(5, r.getHeight());
	}
}
