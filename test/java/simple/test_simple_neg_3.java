
public class Main {

	@Test
	public void main() {
		Rectangle r = new Rectangle();
		int testWidth = 5;
		r.setWidth(testWidth);
		assertEquals(testWidth, r.getWidth());

		int testHeight = 10;
		r.setHeight(testHeight);
		assertEquals(testHeight, r.getHeight());
	}
}
