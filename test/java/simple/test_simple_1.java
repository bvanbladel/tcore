
public class Main {

    @Test
    public void main() {
        Rectangle r = new Rectangle();
        r.setWidth(5);
        r.setHeight(10);
        assertEquals(5, r.getWidth());
        assertEquals(10, r.getHeight());
    }
}
