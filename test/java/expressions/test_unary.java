
public class Main {

	@Test
	public void main() {
        int testWidth = 4;
		int testHeight = testWidth + 7;
        ++testWidth;
        testHeight--;
		Rectangle r = new Rectangle();
		r.setWidth(testWidth);
		r.setHeight(testHeight);
		assertEquals(testWidth, r.getWidth());
		assertEquals(testHeight, r.getHeight());
	}
}
