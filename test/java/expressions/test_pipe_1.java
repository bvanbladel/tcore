
public class Main {

	@Test
	public void main() {
        int testWidth = 5;
		int testHeight = 10;
		Rectangles rs = new Rectangles();
        Rectangle r = new Rectangle();
        r.setWidth(testWidth);
        r.setHeight(testHeight);
        rs.addRectangle(r);
		assertEquals(testWidth, rs.getRectangles()[0].getWidth());
		assertEquals(testHeight, rs.getRectangles()[0].getHeight());
	}
}
