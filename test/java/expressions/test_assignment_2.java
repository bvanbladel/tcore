
public class Main {

	@Test
	public void main() {
        int testWidth = 0;
		int testHeight = 15;
        testWidth += 5;
        testHeight -= 5;
		Rectangle r = new Rectangle();
		r.setWidth(testWidth);
		r.setHeight(testHeight);
		assertEquals(testWidth, r.getWidth());
		assertEquals(testHeight, r.getHeight());
	}
}
