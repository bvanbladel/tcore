
public class Main {

	@Test
	public void main() {
        int testWidth = 5;
		int testHeight = testWidth == 5 ? 10 : 0;
		Rectangle r = new Rectangle();
		r.setWidth(testWidth);
		r.setHeight(testHeight);
		assertEquals(testWidth, r.getWidth());
		assertEquals(testHeight, r.getHeight());
	}
}
