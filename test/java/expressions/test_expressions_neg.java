
public class Main {

	@Test
	public void main() {
        int testWidth = 1;
        int testHeight = ((++testWidth) * 2) + ((testWidth--) * 3);
        testWidth = testWidth + 2;
        testWidth++;
		Rectangle r = new Rectangle();
		r.setWidth(testWidth);
		r.setHeight(testHeight);
		assertEquals(testWidth, r.getWidth());
		assertEquals(testHeight, r.getHeight());
	}
}
