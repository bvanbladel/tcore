#!/bin/bash

make

rm error_*.log
rm timeout_*.log
rm dataset/*.xml

rm dataset.txt
touch dataset.txt
ls dataset > dataset.txt

function test {
for testfile in $1
do
    python3 src/main.py -v -o xml -i dataset/$testfile -n ${testfile%.*}
    success=`echo $?`

    if ((0 == $success))
    then
        mv ${testfile%.*}.xml "dataset/${testfile%.*}.xml"
    fi
done
}

for datapoint in `cat dataset.txt`
do
    processes=`ps | grep "src/main.py" | wc -l`
    while ((4 < ${processes} ))
    do
        sleep 1
        processes=`ps | grep "src/main.py" | wc -l`
    done
    test "${datapoint}"&
done
