#!/bin/bash

green='\033[0;32m'
red='\033[0;31m'
NC='\033[0m' # No Color

echo "" > test.log
echo -e "${green}[==========]${NC} Running tests."
echo -e "${green}[----------]${NC}"
echo -e "${green}[----------]${NC}"

totalam=0
succesam=0

function test {
    let totalam=totalam+1
   	echo -e "${green}[ RUN      ]${NC} "$1
   	echo "=====================================" >> test.log
   	echo "Test case: "$1 >> test.log
   	echo "=====================================" >> test.log
    echo "" >> test.log

    python3 src/main.py -i $1 -a -f setupWidth setupHeight setupData >> test.log 2>&1
    succes=`echo $?`

    if ((0 == $succes))
    then
        diff AST.dot "${1%.*}_AST.dot" >> test.log 2>&1
        succes=`echo $?`
    fi

    if ((0 == $succes))
    then
        diff TBT.dot "${1%.*}_TBT.dot" >> test.log 2>&1
        succes=`echo $?`
    fi

    rm AST.dot >> test.log 2>&1
    rm TBT.dot >> test.log 2>&1

    if ((0 == $succes))
   	then
        echo "OK" >> test.log
        echo "" >> test.log
   		echo -e "${green}[       OK ]${NC} "$1
   		let succesam=succesam+1
   	else
   		echo -e "${red}[  FAILED  ]${NC} "$1
   	fi
}

function testopensource {
    let totalam=totalam+1
    testname=`echo ${cmd} | grep -o "test/java/opensource/[a-zA-Z0-9]*"`
   	echo -e "${green}[ RUN      ]${NC} "${testname}".java"
   	echo "=====================================" >> test.log
   	echo "Test case: "${testname}".java" >> test.log
   	echo "=====================================" >> test.log
    echo "" >> test.log

    $1 >> test.log
    succes=`echo $?`

    if ((0 == $succes))
    then
        diff AST.dot "${testname}_AST.dot" >> test.log 2>&1
        succes=`echo $?`
    fi

    if ((0 == $succes))
    then
        diff TBT.dot "${testname}_TBT.dot" >> test.log 2>&1
        succes=`echo $?`
    fi

    rm AST.dot >> test.log 2>&1
    rm TBT.dot >> test.log 2>&1

    if ((0 == $succes))
   	then
        echo "OK" >> test.log
        echo "" >> test.log
   		echo -e "${green}[       OK ]${NC} "${testname}".java"
   		let succesam=succesam+1
   	else
   		echo -e "${red}[  FAILED  ]${NC} "${testname}".java"
   	fi
}


# for case in test/cpp/*.cpp
# do
#     test $case
# done
#
# echo -e "${green}[----------]${NC}"
#
# for case in test/cpp/expressions/*.cpp
# do
#     test $case
# done
#
# echo -e "${green}[----------]${NC}"
#
# for case in test/cpp/functionsAndConditionals/*.cpp
# do
#     test $case
# done
#
# echo -e "${green}[----------]${NC}"

for case in test/java/simple/*.java
do
    test $case
done

echo -e "${green}[----------]${NC}"

for case in test/java/junit3/*.java
do
    test $case
done

echo -e "${green}[----------]${NC}"

for case in test/java/expressions/*.java
do
    test $case
done

echo -e "${green}[----------]${NC}"

for case in test/java/arrays/*.java
do
    test $case
done

echo -e "${green}[----------]${NC}"

for case in test/java/conditionals/*.java
do
    test $case
done

echo -e "${green}[----------]${NC}"

for case in test/java/loops/*.java
do
    test $case
done

echo -e "${green}[----------]${NC}"

for case in test/java/functions/*.java
do
    test $case
done

echo -e "${green}[----------]${NC}"

for case in test/java/exceptions/*.java
do
    test $case
done

echo -e "${green}[----------]${NC}"

while read cmd
do
    testopensource "$cmd"
done < test/java/opensource/commands

echo -e "${green}[----------]${NC}"
echo -e "${green}[----------]${NC}"
echo -e "${green}[==========]${NC} ${totalam} tests ran."
if ((1 == $succesam))
then
  echo -e "${green}[  PASSED  ]${NC} ${succesam} test."
fi
if ((1 < $succesam))
then
	echo -e "${green}[  PASSED  ]${NC} ${succesam} tests."
fi
if (($totalam > $succesam))
then
	let failedam=totalam-succesam
    echo -e "${red}[  FAILED  ]${NC} ${failedam} tests."
fi
