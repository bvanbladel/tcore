"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from AST.Node import Node

class TBTComparer():
    """ Compares two TBTs
    """

    def __init__(self, tbt1:Node, tbt2:Node):
        """ Constructor """
        self.tbt1_asserts = tbt1.getChildNodes()
        self.tbt2_asserts = tbt2.getChildNodes()
        self.matched_asserts = {}

    def compare(self):
        """ Compare the TBTs """
        for node1 in self.tbt1_asserts:
            for node2 in self.tbt2_asserts:
                if node1 == node2:
                    self.matched_asserts[node1] = node2

    def getStatistics(self):
        """ Get the percentage of matching asserts """
        return int((100 * len(list(self.matched_asserts.keys()))) / len(self.tbt1_asserts))

    def getCodeFromFile(self, file, start, end, nr):
        """ Get specific lines from a file """
        html_code = '<div class="col-sm-6"><pre class="pre_scrollable text-left"><code>'
        with open(file) as fp:
            for i, line in enumerate(fp):
                if (i+1) == nr:
                    html_code += '<mark class="bg-success">'
                if (i+1) >= start and (i+1) <= end:
                    html_code += line
                elif (i+1) > end:
                    break
                if (i+1) == nr:
                    html_code += '</mark>'
        html_code += '</code></pre></div>'
        return html_code

    def getOverviewFromFile(self, file, posnrs, negnrs):
        """ Get overview from a file """
        html_code = '<div class="col-sm-6"><pre class="pre_scrollable text-left"><code>'
        with open(file) as fp:
            for i, line in enumerate(fp):
                if (i+1) in posnrs:
                    html_code += '<mark class="bg-success">'
                elif (i+1) in negnrs:
                    html_code += '<mark class="bg-danger">'

                html_code += line

                if (i+1) in posnrs or (i+1) in negnrs:
                    html_code += '</mark>'
        html_code += '</code></pre></div>'
        return html_code

    def getHTMLReport(self, file1, file2):
        """ Get the details of the matching asserts, in HTML format """
        html_report = ""

        # Preparation
        posnrs1 = []
        negnrs1 = []
        for node in self.tbt1_asserts:
            if node in self.matched_asserts.keys():
                posnrs1.append(node.trace['lineNr'])
            else:
                negnrs1.append(node.trace['lineNr'])
        posnrs2 = []
        negnrs2 = []
        for node in self.tbt2_asserts:
            if node in self.matched_asserts.values():
                posnrs2.append(node.trace['lineNr'])
            else:
                negnrs2.append(node.trace['lineNr'])

        # Overview
        html_report += '<div class="card mt-3"><h5 class="card-header">Overview</h5><div class="card-body"><div class="container"><div class="row">'
        html_report += self.getOverviewFromFile(file1, posnrs1, negnrs1)
        html_report += self.getOverviewFromFile(file2, posnrs2, negnrs2)
        html_report += '</div></div></div></div>'

        # Traceability report
        html_report += '<div class="card mt-3"><h5 class="card-header">Traceability</h5><ul class="list-group list-group-flush">'
        for node in list(self.matched_asserts.keys()):
            html_report += '<li class="list-group-item"><div class="container"><div class="row">'
            html_report += self.getCodeFromFile(file1, node.old_parent.trace['lineNrStart'], node.old_parent.trace['lineNrEnd'], node.trace['lineNr'])
            html_report += self.getCodeFromFile(file2, self.matched_asserts[node].old_parent.trace['lineNrStart'], self.matched_asserts[node].old_parent.trace['lineNrEnd'], self.matched_asserts[node].trace['lineNr'])
            html_report += '</div></div></li>'
        html_report += '</ul></div>'
        return html_report
