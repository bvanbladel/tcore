"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from copy import deepcopy
from treehandler.Visitor import Visitor
from AST.Node import Node
from AST.LiteralNode import LiteralNode
from AST.VariableDeclarationNode import VariableDeclarationNode
from AST.FunctionDefinitionNode import FunctionDefinitionNode
from AST.ClassDefinitionNode import ClassDefinitionNode
from AST.FunctionCallNode import FunctionCallNode
from AST.BlockNode import BlockNode
from AST.AssertNode import AssertNode

class PreprocessingVisitor(Visitor):
    """ Visitor that preprocesses the AST
    """

    def __init__(self):
        """ Constructor """
        self.datamembers = []
        self.before = []
        self.after = []

    def visit(self, node:Node):
        """ Function that visits a node"""
        self.enterNode(node)
        self.exitNode(node)

    def enterNode(self, node:Node):
        """ Function that is called upon entering a node during visit """
        if isinstance(node, VariableDeclarationNode):
            self.handleVariableDeclarationEnter(node)
        elif isinstance(node, FunctionDefinitionNode):
            self.handleFunctionDefinitionEnter(node)
        elif (type(node) == Node or isinstance(node, ClassDefinitionNode)):
            for child in node.getChildNodes():
                child.accept(self)
        else:
            pass

    def exitNode(self, node:Node):
        """ Function that is called upon exiting a node during visit """
        if isinstance(node, ClassDefinitionNode):
            self.handleClassDefinitionNodeExit(node)
        else:
            pass

    def handleVariableDeclarationEnter(self, node:VariableDeclarationNode):
        """ Handle entering a VariableDeclarationNode """
        self.datamembers.append(node)

    def handleFunctionDefinitionEnter(self, node:FunctionDefinitionNode):
        """ Handle entering a FunctionDefinitionNode """
        if ("Before" in node.getDeclaration().getAnnotations() or ("setup" == node.getDeclaration().getName().lower() and "testcase" in node.getParent().getDeclaration().getName().lower())):
            self.before = node.getChildNodes()[1].getChildNodes()[::-1]
        elif ("After" in node.getDeclaration().getAnnotations() or ("teardown" == node.getDeclaration().getName().lower() and "testcase" in node.getParent().getDeclaration().getName().lower())):
            self.after = node.getChildNodes()[1].getChildNodes()
        elif ("Test" in node.getDeclaration().getAnnotations() or ("test" == node.getDeclaration().getName()[:4].lower() and "testcase" in node.getParent().getDeclaration().getName().lower())):
            functionBody = node.getChildNodes()[1]
            for statement in self.before:
                functionBody.insertChild(deepcopy(statement))
            for datamember in self.datamembers:
                functionBody.insertChild(deepcopy(datamember))
            for statement in self.after:
                functionBody.addChild(deepcopy(statement))

    def handleClassDefinitionNodeExit(self, node:ClassDefinitionNode):
        """ Handle exiting a ClassDefinitionNode """
        self.datamembers = []
        self.before = []
        self.after = []
