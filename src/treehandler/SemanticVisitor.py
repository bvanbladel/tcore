"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from utils.Logger import Logger
from treehandler.Visitor import Visitor
from AST.Node import Node
from AST.BlockNode import BlockNode
from AST.BreakNode import BreakNode
from AST.WhileNode import WhileNode
from AST.ContinueNode import ContinueNode
from AST.LiteralNode import LiteralNode
from AST.BasicTypeNodes import *
from AST.PointerTypeNode import PointerTypeNode
from AST.ArrayTypeNode import ArrayTypeNode
from AST.VariableNode import VariableNode
from AST.FunctionDefinitionNode import FunctionDefinitionNode
from AST.FunctionCallNode import *
from AST.FunctionDeclarationNode import FunctionDeclarationNode, IOFunctionDeclarationNode
from AST.VariableDeclarationNode import VariableDeclarationNode
from AST.BinaryOperatorNode import BinaryOperatorNode, CompareOperatorNode
from AST.UnaryOperatorNode import UnaryOperatorNode
from AST.NestedExpressionNode import NestedExpressionNode
from AST.ArrayAccessNode import ArrayAccessNode
from AST.AssignmentNode import AssignmentNode
from AST.ReturnNode import ReturnNode
from AST.IncludeNode import IncludeNode
from AST.IdentifierNode import IdentifierNode
from AST.ParameterNode import ParameterNode
from AST.ConversionNodes import *
from SymbolTable.SymbolTable import SymbolTable

class SemanticVisitor(Visitor):
    """ Visitor of the AST that checks for semantic errors

    This visitor will check for semantic errors and also construct
    the symbol table. 'Pointers' to the declaration node will be saved
    in variable nodes.
    """

    def __init__(self, logger:Logger):
        """ Constructor """
        self.symboltable = SymbolTable()
        self.logger = logger
        self.localsCount = 0

    def visit(self, node:Node):
        """ Function that visits a node"""
        self.enterNode(node)
        for child in node.getChildNodes():
            child.accept(self)
        self.exitNode(node)

    def getMain(self):
        """ Return the node of the main function """
        if not self.symboltable.exists("main"):
            self.logger.error({'lineNr': 0, 'charPos': 0, 'symbol': "main"}, "Undefined reference to main")
        else:
            return self.symboltable.retrieveSymbol('main').definition.getChildNodes()[1]

    def enterNode(self, node:Node):
        """ Function that is called upon entering a node during visit """
        if isinstance(node, BlockNode) and not isinstance(node.parent, FunctionDefinitionNode):
            self.symboltable.openScope()
        elif isinstance(node, FunctionDefinitionNode):
            self.handleFunctionDefinition(node)
        elif isinstance(node, FunctionDeclarationNode):
            self.handleFunctionDeclaration(node)
        elif isinstance(node, VariableDeclarationNode):
            self.handleVariableDeclaration(node)
        elif isinstance(node, LiteralNode):
            self.handleLiteral(node)
        elif isinstance(node, VariableNode):
            self.handleVariable(node)
        elif isinstance(node, ReturnNode):
            self.handleReturn(node)
        elif isinstance(node, BreakNode) or isinstance(node, ContinueNode):
            self.handleBreakAndContinue(node)
        elif isinstance(node, IncludeNode):
            self.handleInclude(node)
        else:
            pass


    def exitNode(self, node:Node):
        """ Function that is called upon exiting a node during visit """
        if isinstance(node, BlockNode) and not isinstance(node.parent, FunctionDefinitionNode):
            self.symboltable.closeScope()
        elif isinstance(node, FunctionDefinitionNode):
            node.localsNr = self.localsCount
            self.symboltable.closeScope()
        elif isinstance(node, BinaryOperatorNode):
            self.handleBinaryOperator(node)
        elif isinstance(node, UnaryOperatorNode):
            self.handleUnaryOperator(node)
        elif isinstance(node, NestedExpressionNode):
            self.handleNestedExpression(node)
        elif isinstance(node, ArrayAccessNode):
            self.handleArrayAccess(node)
        elif isinstance(node, AssignmentNode):
            self.handleAssignment(node)
        elif isinstance(node, VariableDeclarationNode):
            self.handleVariableDeclarationExit(node)
        elif isinstance(node, FunctionCallNode):
            self.handleFunctionCall(node)
        elif isinstance(node, ReturnNode):
            self.handleReturnExit(node)
        else:
            pass

    def handleFunctionDeclaration(self, node:FunctionDeclarationNode):
        """ Function that will handle function declaration """
        name = node.getName()

        # Check name
        if (self.symboltable.exists(name)):
            entry = self.symboltable.retrieveSymbol(name)

            # Check return type and parameters
            if (entry.declaration == node):
                pass # everything ok, same declaration, no error
            else:
                self.logger.error(node.trace, "conflicting types for '%s'" % name)
                self.logger.note(entry.declaration.trace, "previous declaration is here")
                return
        else:
            conflictingParams = node.getConflictingParameters()
            if (len(conflictingParams)):
                for conflict in conflictingParams:
                    self.logger.error(conflict[-1].trace, "redefinition of parameter '%s'" % conflict[-1].getName())
                    self.logger.note(conflict[0].trace, "previous declaration is here")
            self.symboltable.enterSymbol(name, node, None)


    def handleFunctionDefinition(self, node:FunctionDefinitionNode):
        """ Function that will handle function definitions """
        self.localsCount = 0
        name = node.getDeclaration().getName()

        if (self.symboltable.scope != SymbolTable.GLOBAL_SCOPE):
            self.logger.error(node.getDeclaration().trace, "function definition is not allowed here")

        # Check name
        if (self.symboltable.exists(name)):
            entry = self.symboltable.retrieveSymbol(name)

            # Check return type and parameters
            if (entry.declaration != node.getDeclaration()):
                pass # error: conflicting types for this function: will be detected in the declaration node
            elif (entry.definition is not None):
                self.logger.error(node.getDeclaration().trace, "redefinition of '%s'" % name)
                self.logger.note(entry.declaration.trace, "previous definition is here")
            else:
                entry.declaration = node.getDeclaration()
                entry.definition = node
        else:
            self.symboltable.enterSymbol(name, node.getDeclaration(), node)

        self.symboltable.openScope()
        conflictingParams = node.getDeclaration().getConflictingParameters()
        if (not conflictingParams):
            params = node.getDeclaration().getParameters()
            for param in params:
                self.symboltable.enterSymbol(param.getName(), param, None)
        else:
            for conflict in conflictingParams:
                self.logger.error(conflict[-1].trace, "redefinition of parameter '%s'" % conflict[-1].getName())
                self.logger.note(conflict[0].trace, "previous declaration is here")



    def handleVariableDeclaration(self, node:VariableDeclarationNode):
        """ Function that will handle variable declaration """
        name = node.getName()
        node.childNodes[1].lhs = True

        # Check name
        if (self.symboltable.existsInCurrentScope(name)):
            entry = self.symboltable.retrieveSymbol(name)

            # if global scope
            if (entry.scope == SymbolTable.GLOBAL_SCOPE):

                # check for definition errors or type errors
                if ((entry.definition is not None) and (node.hasDefinition())) or (entry.declaration.getType() != node.getType()):
                    typediff = ""
                    if (entry.declaration.getType() != node.getType()):
                        typediff = " with a different type: '%s' vs '%s'" % (node.getType(), entry.declaration.getType())
                    self.logger.error(node.trace, "redefinition of '%s'%s" % (name, typediff))
                    self.logger.note(entry.declaration.trace, "previous definition is here")
                    return

                entry.declaration = node
                entry.definition = node.getDefinition()

            else: # local scope = redeclaration
                typediff = ""
                if (entry.declaration.getType() != node.getType()):
                    typediff = " with a different type: '%s' vs '%s'" % (node.getType(), entry.declaration.getType())
                self.logger.error(node.trace, "redefinition of '%s'%s" % (name, typediff))
                self.logger.note(entry.declaration.trace, "previous definition is here")

        else:
            if (node.hasDefinition()): #definition
                self.symboltable.enterSymbol(name, node, node.getDefinition())
            else: #declaration
                self.symboltable.enterSymbol(name, node, None)


    def handleVariableDeclarationExit(self, node:VariableDeclarationNode):
        """ Function that will handle variable declaration exit """
        if (node.hasDefinition()):
            # check if assignment types match
            if (node.getTypeNode() != node.childNodes[2].exprtype):
                if not self.insertConversionNode(node.childNodes[2], node.getTypeNode()):
                    self.logger.error(node.trace, "incompatible types: assigning '%s' to '%s'" % (node.childNodes[2].exprtype, node.getTypeNode()))
            self.localsCount += 1 # count the memory required for the variable
        else:
            # if array declaration
            if isinstance(node.getTypeNode(), ArrayTypeNode):
                # check if size is int
                if node.getTypeNode().size is None:
                    self.logger.error(node.trace, "size of array has non-integer type")
                # if everything is ok, count the memory required for all elements
                else:
                    self.localsCount += node.getTypeNode().size

            # if non-array declaration
            else:
                self.localsCount += 1 # count the memory required for the variable



    def handleLiteral(self, node:LiteralNode):
        """ Function that will handle a literal """
        literal = node.content
        if (literal[0] == "'" and literal[-1] == "'"):
            node.setType(CharTypeNode())
        elif (literal[0] == '"' and literal[-1] == '"'):
            t = ArrayTypeNode(len(node.content[1:-1]))
            t.addChild(CharTypeNode())
            node.setType(t)
        elif ('.' in literal or literal[-1] == 'f'):
            node.setType(FloatTypeNode())
        else:
            node.setType(IntTypeNode())


    def handleVariable(self, node:VariableNode):
        """ Function that will handle a variable """
        name = node.getName()
        if (self.symboltable.exists(name)):
            entry = self.symboltable.retrieveSymbol(name)
            node.setType(entry.declaration.getTypeNode())
            node.declaration = entry.declaration
        else:
            self.logger.error(node.trace, "use of undeclared identifier '%s'" % name)


    def handleFunctionCall(self, node:FunctionCallNode):
        """ Function that will handle a function call """
        name = node.getName()
        if (self.symboltable.exists(name)):
            entry = self.symboltable.retrieveSymbol(name)
            if (entry.declaration.checkArgumentLengthToFew(node)):
                if not isinstance(entry.declaration, IOFunctionDeclarationNode):
                    self.logger.error(node.trace, "too few arguments to function call, expected %d, have %d" % (len(entry.declaration.getParameters()), len(node.getArguments())))
                    self.logger.note(entry.declaration.trace, "'%s' declared here" % name)
                else:
                    self.logger.error(node.trace, "too few arguments to function call")
            elif (entry.declaration.checkArgumentLengthToMany(node)):
                if not isinstance(entry.declaration, IOFunctionDeclarationNode):
                    self.logger.error(node.trace, "too many arguments to function call, expected %d, have %d" % (len(entry.declaration.getParameters()), len(node.getArguments())))
                    self.logger.note(entry.declaration.trace, "'%s' declared here" % name)
                else:
                    self.logger.error(node.trace, "too many arguments to function call")
            else:
                args = node.getArguments()
                argtypes = entry.declaration.getArgumentTypes()
                for i in range(0, len(argtypes)):
                    if (args[i].exprtype != argtypes[i]) and (not self.insertConversionNode(args[i], argtypes[i])):
                        self.logger.error(node.trace, "incompatible types of arguments when calling '%s'" % (name))
                        if not isinstance(entry.declaration, IOFunctionDeclarationNode):
                            self.logger.note(entry.declaration.trace, "'%s' declared here" % name)
                        return
                if isinstance(entry.declaration, IOFunctionDeclarationNode) and (entry.declaration.checkFormatString(node)):
                    self.logger.error(node.trace, "incompatible types of arguments when calling '%s'" % (name))

                node.setType(entry.declaration.getTypeNode())
                node.declaration = entry.declaration
        else:
            self.logger.error(node.trace, "use of undeclared function '%s'" % name)

        if isinstance(node, FunctionMemberCallNode):
            node.getObject().lhs = True

    def handleBinaryOperator(self, node:BinaryOperatorNode):
        """ Function that will handle a binary operation """
        # if comparison (and, or, ==, >, <, ...)
        if isinstance(node, CompareOperatorNode):

            # if arraytype node: error, cannot compare arrays
            if isinstance(node.childNodes[0].exprtype, ArrayTypeNode) or isinstance(node.childNodes[1].exprtype, ArrayTypeNode):
                self.logger.error(node.trace, "invalid operation '%s' for type '%s'" % (node, ArrayTypeNode(0)))

            # if types are not equal when comparing (not including AND and OR, they take any type)
            elif (node.childNodes[0].exprtype != node.childNodes[1].exprtype) and node.operator not in ['&&', '||']:
                # if one of the nodes is a float, try to convert to floats, error on fail
                if isinstance(node.childNodes[0].exprtype, FloatTypeNode):
                    if not self.insertConversionNode(node.childNodes[1], FloatTypeNode()):
                        self.logger.error(node.trace, "incompatible types for %s operation: '%s' vs '%s'" % (node, node.childNodes[0].exprtype, node.childNodes[1].exprtype))
                elif isinstance(node.childNodes[1].exprtype, FloatTypeNode):
                    if not self.insertConversionNode(node.childNodes[0], FloatTypeNode()):
                        self.logger.error(node.trace, "incompatible types for %s operation: '%s' vs '%s'" % (node, node.childNodes[0].exprtype, node.childNodes[1].exprtype))
                # else, desperately try to convert second node to type of first node, error on fail
                else:
                    if not self.insertConversionNode(node.childNodes[1], node.childNodes[0].exprtype):
                        self.logger.error(node.trace, "incompatible types for %s operation: '%s' vs '%s'" % (node, node.childNodes[0].exprtype, node.childNodes[1].exprtype))

            # if AND or OR operation: convert to bool
            elif (node.operator in ['&&', '||']):
                self.insertConversionNode(node.childNodes[0])
                self.insertConversionNode(node.childNodes[1])

            # comparison always returns 0 or 1 (int)
            node.setType(IntTypeNode())

        # if operation (+,-,*,/,...)
        else:
            # if array type node: error
            if isinstance(node.childNodes[0].exprtype, ArrayTypeNode) or isinstance(node.childNodes[1].exprtype, ArrayTypeNode):
                self.logger.error(node.trace, "invalid operation '%s' for type '%s'" % (node, ArrayTypeNode(0)))
                node.setType(node.childNodes[0].exprtype)
            # if pointer type node: error
            elif  isinstance(node.childNodes[0].exprtype, PointerTypeNode) or isinstance(node.childNodes[1].exprtype, PointerTypeNode):
                self.logger.error(node.trace, "invalid operation '%s' for type '%s'" % (node, PointerTypeNode()))
                node.setType(node.childNodes[0].exprtype)

            # if types are not equal
            elif (node.childNodes[0].exprtype != node.childNodes[1].exprtype):
                # if one of the nodes is a float, try to convert to floats, error on fail
                if isinstance(node.childNodes[0].exprtype, FloatTypeNode):
                    if not self.insertConversionNode(node.childNodes[1], FloatTypeNode()):
                        self.logger.error(node.trace, "incompatible types for %s operation: '%s' vs '%s'" % (node, node.childNodes[0].exprtype, node.childNodes[1].exprtype))
                    node.setType(FloatTypeNode())
                elif isinstance(node.childNodes[1].exprtype, FloatTypeNode):
                    if not self.insertConversionNode(node.childNodes[0], FloatTypeNode()):
                        self.logger.error(node.trace, "incompatible types for %s operation: '%s' vs '%s'" % (node, node.childNodes[0].exprtype, node.childNodes[1].exprtype))
                    node.setType(FloatTypeNode())
                # else, desperately try to convert second node to type of first node, error on fail
                else:
                    if not self.insertConversionNode(node.childNodes[1], node.childNodes[0].exprtype):
                        self.logger.error(node.trace, "incompatible types for %s operation: '%s' vs '%s'" % (node, node.childNodes[0].exprtype, node.childNodes[1].exprtype))
                    node.setType(node.childNodes[0].exprtype)

            # else: everything OK
            else:
                node.setType(node.childNodes[0].exprtype)


    def handleNestedExpression(self, node:NestedExpressionNode):
        """ Function that will handle a nested expression """
        node.setType(node.childNodes[0].exprtype)


    def handleUnaryOperator(self, node:UnaryOperatorNode):
        """ Function that will handle a unary operation """
        if (node.operator == "*"):
            if isinstance(node.childNodes[0].exprtype, PointerTypeNode):
                node.setType(node.childNodes[0].exprtype.childNodes[0])
            else:
                self.logger.error(node.trace, "trying to dereference non-pointer type: '%s'" % (node.childNodes[0].exprtype))
                node.setType(node.childNodes[0].exprtype)

        elif (node.operator == "&"):
            if isinstance(node.childNodes[0], VariableNode) or isinstance(node.childNodes[0], ArrayAccessNode):
                node.childNodes[0].lhs = True
                p = PointerTypeNode()
                p.addChild(node.childNodes[0].exprtype)
                node.setType(p)
            else:
                self.logger.error(node.trace, "cannot take the address of an rvalue of type '%s'" % (node.childNodes[0].exprtype))
                p = PointerTypeNode()
                p.addChild(node.childNodes[0].exprtype)
                node.setType(p)

        elif (node.operator == "!"):
            self.insertConversionNode(node.childNodes[0])
            node.setType(node.childNodes[0].exprtype)

        elif (node.operator == "++" or node.operator == "--"):
                if isinstance(node.childNodes[0], VariableNode) or isinstance(node.childNodes[0], ArrayAccessNode) or (isinstance(node.childNodes[0], UnaryOperatorNode) and node.childNodes[0].operator == "*"):
                    node.childNodes[0].lhs = True
                    if (isinstance(node.childNodes[0], UnaryOperatorNode) and node.childNodes[0].operator == "*"):
                        node.childNodes[0].childNodes[0].lhs = True
                    node.setType(node.childNodes[0].exprtype)
                else:
                    self.logger.error(node.trace, "expression is not assignable")
                    node.setType(node.childNodes[0].exprtype)

        else:
            node.setType(node.childNodes[0].exprtype)


    def handleArrayAccess(self, node:ArrayAccessNode):
        """ Function that will handle an array access """
        node.childNodes[0].lhs = True
        if isinstance(node.childNodes[0].exprtype, ArrayTypeNode):
            if isinstance(node.childNodes[1].exprtype, IntTypeNode):
                node.setType(node.childNodes[0].exprtype.childNodes[0])
            else:
                self.logger.error(node.trace, "array subscript is non-integer type '%s'" % (node.childNodes[1].exprtype))
        else:
            self.logger.error(node.trace, "subscripted value is non-array type: '%s'" % (node.childNodes[0].exprtype))
            node.setType(node.childNodes[0].exprtype)


    def handleAssignment(self, node:AssignmentNode):
        """ Function that will handle assignments """
        var = node.childNodes[0]
        var.lhs = True
        if isinstance(var, UnaryOperatorNode): #dereference
            var.childNodes[0].lhs = True

        if (var.exprtype != node.childNodes[1].exprtype):
            if not self.insertConversionNode(node.childNodes[1], var.exprtype):
                self.logger.error(node.trace, "incompatible types: assigning '%s' to '%s'" % (node.childNodes[1].exprtype, var.exprtype))


    def handleReturn(self, node:ReturnNode):
        """ Function that will handle return statements """
        n = node
        while(not isinstance(n, FunctionDefinitionNode)):
            if (n.parent == None):
                self.logger.error(node.trace, "return statement outside function scope")
                return
            n = n.parent
        node.function = n.getDeclaration()


    def handleReturnExit(self, node:ReturnNode):
        """ Function that will handle return statements exit """
        if (node.function != None):
            if len(node.childNodes) and (node.function.getTypeNode() != node.childNodes[0].exprtype):
                if not self.insertConversionNode(node.childNodes[0], node.function.getTypeNode()):
                    self.logger.error(node.trace, "incompatible types: returning wrong type: '%s' vs '%s'" % (node.childNodes[0].exprtype, node.function.getTypeNode()))
                    self.logger.note(node.function.trace, "function '%s' with type '%s' declared here: " % (node.function.getName(), node.function.getType()))
            elif not len(node.childNodes) and (node.function.getTypeNode() != VoidTypeNode()):
                self.logger.error(node.trace, "incompatible types: returning wrong type: '%s' vs '%s'" % (VoidTypeNode(), node.function.getTypeNode()))
                self.logger.note(node.function.trace, "function '%s' with type '%s' declared here: " % (node.function.getName(), node.function.getType()))


    def handleBreakAndContinue(self, node:Node):
        """ Function that will handle break and continue statements """
        n = node
        while(not isinstance(n, WhileNode)):
            if (n.parent == None):
                self.logger.error(node.trace, "'%s' statement not in loop statement" % str(node))
                return
            n = n.parent
        node.loop = n


    def handleInclude(self, node:IncludeNode):
        """ Function that will handle include statements """
        if (node.include != "stdio.h"):
            self.logger.error(node.trace, "%s: No such file or directory" % (node.include))
        else:
            self.symboltable.enterSymbol("printf", self.getIOFunction("printf"), None)
            self.symboltable.enterSymbol("scanf", self.getIOFunction("scanf"), None)


    def getIOFunction(self, functionname:str):
        """ Generate declaration nodes for IO functions """
        declaration = IOFunctionDeclarationNode()
        declaration.addChild(IntTypeNode())
        declaration.addChild(IdentifierNode(functionname))
        parameter1 = ParameterNode()
        parameter1.addChild(PointerTypeNode())
        parameter1.childNodes[0].addChild(CharTypeNode())
        parameter1.addChild(IdentifierNode("format"))
        declaration.addChild(parameter1)
        return declaration


    def insertConversionNode(self, node:Node, typenode:TypeNode=None):
        """ Insert type conversion node between 'node' and the parent node of 'node' (return true on success)"""
        parent = node.parent
        childIndex = 0
        for index in range(0, len(parent.childNodes)):
            if id(node) == id(parent.childNodes[index]):
                childIndex = index
                break
        if isinstance(node.exprtype, IntTypeNode) and isinstance(typenode, FloatTypeNode):
            conv = IntToFloatNode()
        elif isinstance(node.exprtype, FloatTypeNode) and isinstance(typenode, IntTypeNode):
            conv = FloatToIntNode()
            self.logger.warning(parent.trace, "implicit conversion turns floating-point number into integer ")
        elif (typenode == None):
            conv = BasicTypeToBoolNode()
            typenode = IntTypeNode()
        else:
            return False

        parent.childNodes[childIndex] = conv
        conv.parent = parent
        conv.addChild(node)
        conv.setType(typenode)
        return True
