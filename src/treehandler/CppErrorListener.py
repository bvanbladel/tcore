"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from antlr4.error.ErrorListener import ErrorListener
from utils.Logger import Logger

class CppErrorListener(ErrorListener):
    """ Listener for Antlr Parser

    This class will generate error messages upon invalid input
    """
    
    def __init__(self, logger:Logger):
        """ Constructor """
        super().__init__()
        self.logger = logger

    def syntaxError(self, recognizer, offendingSymbol, lineNr, charPos, msg, exception):
        """ Syntax error handler """
        self.logger.error({'lineNr': lineNr, 'charPos': charPos, 'symbol': offendingSymbol.text}, "syntax error at symbol '%s'" % offendingSymbol.text)

        
        
