"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import re
from copy import deepcopy

from generated.JavaListener import JavaListener
from generated.JavaParser import JavaParser
from AST.Node import Node
from AST.IncludeNode import IncludeNode
from AST.ReturnNode import ReturnNode
from AST.ConstNode import ConstNode
from AST.IfNode import IfNode
from AST.IfElseNode import IfElseNode
from AST.WhileNode import WhileNode
from AST.BreakNode import BreakNode
from AST.ContinueNode import ContinueNode
from AST.FunctionDefinitionNode import FunctionDefinitionNode
from AST.BlockNode import BlockNode
from AST.VariableDeclarationNode import VariableDeclarationNode
from AST.FunctionDeclarationNode import FunctionDeclarationNode
from AST.BasicTypeNodes import *
from AST.PointerTypeNode import PointerTypeNode
from AST.ArrayTypeNode import ArrayTypeNode
from AST.CustomTypeNode import CustomTypeNode
from AST.IdentifierNode import IdentifierNode
from AST.LiteralNode import LiteralNode
from AST.ParameterNode import ParameterNode
from AST.AssignmentNode import AssignmentNode
from AST.NestedExpressionNode import NestedExpressionNode
from AST.ArrayAccessNode import ArrayAccessNode
from AST.VariableNode import VariableNode
from AST.UnaryOperatorNode import UnaryOperatorNode
from AST.BinaryOperatorNode import BinaryOperatorNode, CompareOperatorNode
from AST.FunctionCallNode import *
from AST.ClassDeclarationNode import ClassDeclarationNode
from AST.ClassDefinitionNode import ClassDefinitionNode
from AST.TempNode import TempNode
from AST.ThrowNode import ThrowNode
from AST.ConversionNode import ConversionNode
from AST.AccessNodes import *
from AST.ArrayNode import ArrayNode
from AST.TryNode import TryNode
from AST.CatchNode import CatchNode
from AST.SizeOfNode import SizeOfNode

class JavaListenerImplementation(JavaListener):
    """ Listener for Antlr Parser

    This class will generate an Abstract Syntax Tree (AST) from
    the antlr parse tree.
    """

    def __init__(self, ):
        """ Constructor """
        self.ast = None
        self.current = None
        self.lastType = ""
        self.functions = {}
        self.annotations = []

    def getAST(self):
        """ Return the constructed AST """
        return self.ast

    def getFunctions(self):
        """ Return the functions dict """
        return self.functions

###############################################################################

    def enterProgram(self, ctx:JavaParser.ProgramContext):
        """ Create the root node of the AST upon entering the program """
        self.ast = Node()
        self.current = self.ast

    def exitProgram(self, ctx:JavaParser.ProgramContext):
        """ Exit the program """
        self.current = None

###############################################################################

    def enterImportStatement(self, ctx:JavaParser.ImportStatementContext):
        """ Enter import statement """
        pass

###############################################################################

    def enterReturnStatement(self, ctx:JavaParser.ReturnStatementContext):
        """ Enter return statement """
        returnNode = ReturnNode()
        returnNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        returnNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        returnNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(returnNode)
        self.current = returnNode

    def exitReturnStatement(self, ctx:JavaParser.ReturnStatementContext):
        """ Exit return statement """
        self.current = self.current.getParent()

###############################################################################

    def enterThrowStatement(self, ctx:JavaParser.ThrowStatementContext):
        """ Enter Throw statement """
        throwNode = ThrowNode()
        throwNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        throwNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        throwNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(throwNode)
        self.current = throwNode

    def exitThrowStatement(self, ctx:JavaParser.ThrowStatementContext):
        """ Exit Throw statement """
        self.current = self.current.getParent()

###############################################################################

    def enterIfStatement(self, ctx:JavaParser.IfStatementContext):
        """ Enter if statement """
        ifNode = IfNode()
        ifNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        ifNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        ifNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(ifNode)
        self.current = ifNode

    def exitIfStatement(self, ctx:JavaParser.IfStatementContext):
        """ Exit if statement """
        self.current = self.current.getParent()

    def enterIfElseStatement(self, ctx:JavaParser.IfElseStatementContext):
        """ Enter if-else statement """
        ifElseNode = IfElseNode()
        ifElseNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        ifElseNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        ifElseNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(ifElseNode)
        self.current = ifElseNode

    def exitIfElseStatement(self, ctx:JavaParser.IfElseStatementContext):
        """ Exit if-else statement """
        self.current = self.current.getParent()

###############################################################################

    def enterTernaryExpression(self, ctx:JavaParser.TernaryExpressionContext):
        """ Enter ternary expression """
        tempNode = TempNode()
        self.current.addChild(tempNode)
        self.current = tempNode

    def exitTernaryExpression(self, ctx:JavaParser.TernaryExpressionContext):
        """ Exit ternary expression """
        # create if else structure
        ifElseNode = IfElseNode()
        ifElseNode.trace['lineNr'] = ctx.getChild(1).getSymbol().line
        ifElseNode.trace['charPos'] = ctx.getChild(1).getSymbol().column
        ifElseNode.trace['symbol'] = ctx.getChild(1).getText()

        # add the condition and options
        ifElseNode.addChild(self.current.childNodes.pop(0))
        ifElseNode.addChild(self.current.childNodes.pop(0))
        ifElseNode.addChild(self.current.childNodes.pop(0))

        self.current.parent.replaceChild(self.current, ifElseNode)
        self.current = ifElseNode.parent

###############################################################################

    def enterWhileStatement(self, ctx:JavaParser.WhileStatementContext):
        """ Enter while statement """
        whileNode = WhileNode()
        whileNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        whileNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        whileNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(whileNode)
        self.current = whileNode

    def exitWhileStatement(self, ctx:JavaParser.WhileStatementContext):
        """ EXit while statement """
        self.current = self.current.getParent()

###############################################################################

    def enterDoWhileStatement(self, ctx:JavaParser.DoWhileStatementContext):
        """ Enter do while statement """
        whileNode = WhileNode()
        whileNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        whileNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        whileNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(whileNode)
        self.current = whileNode

    def exitDoWhileStatement(self, ctx:JavaParser.DoWhileStatementContext):
        """ EXit do while statement """
        if len(self.current.childNodes) == 2:
            self.current.childNodes.reverse()
        self.current = self.current.getParent()

###############################################################################

    def enterForStatement(self, ctx:JavaParser.ForStatementContext):
        """ Enter for statement """
        whileNode = WhileNode()
        whileNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        whileNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        whileNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(whileNode)
        self.current = whileNode

    def exitForStatement(self, ctx:JavaParser.ForStatementContext):
        """ Exit for statement """
        #init statement should be executed before the while
        init = self.current.childNodes.pop(0)
        parent = self.current.parent
        parent.childNodes.insert(parent.childNodes.index(self.current), init)
        init.parent = parent

        #increment should be last statement of block
        block = self.current.childNodes[-1]
        increment = self.current.childNodes.pop(1)
        block.addChild(increment)

        self.current = self.current.getParent()

###############################################################################

    def enterRangedForStatement(self, ctx:JavaParser.RangedForStatementContext):
        """ Enter ranged for statement """
        whileNode = WhileNode()
        whileNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        whileNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        whileNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(whileNode)
        self.current = whileNode

    def exitRangedForStatement(self, ctx:JavaParser.RangedForStatementContext):
        """ Exit ranged for statement """
        #init statement should be executed before the while
        init = VariableDeclarationNode()
        parent = self.current.parent
        init.addChild(IntTypeNode())
        indexvar = VariableNode()
        indexvar.addChild(IdentifierNode("index_ranged_for_%d_%d" % (self.current.trace['lineNr'], self.current.trace['charPos'])))
        init.addChild(indexvar)
        init.addChild(LiteralNode("0"))
        parent.childNodes.insert(parent.childNodes.index(self.current), init)
        init.parent = parent

        #condition should be added
        cond = CompareOperatorNode("<")
        cond.addChild(deepcopy(indexvar))
        sizeoflist = SizeOfNode()
        listvar = self.current.childNodes[-2]
        self.current.childNodes.pop(-2)
        sizeoflist.addChild(listvar)
        cond.addChild(sizeoflist)
        self.current.childNodes.insert(0, cond)
        self.current.childNodes[0].parent = self.current

        #loopvar should be the first statement of block
        loopvardecl = VariableDeclarationNode()
        loopvartype = ctx.getChild(2).getText()
        if (loopvartype == "int"):
            loopvardecl.addChild(IntTypeNode())
        elif (loopvartype == "float"):
            loopvardecl.addChild(FloatTypeNode())
        elif (loopvartype == "double"):
            loopvardecl.addChild(DoubleTypeNode())
        elif (loopvartype == "char"):
            loopvardecl.addChild(CharTypeNode())
        elif (loopvartype == "void"):
            loopvardecl.addChild(VoidTypeNode())
        elif (loopvartype == "boolean"):
            loopvardecl.addChild(BoolTypeNode())
        else:
            loopvardecl.addChild(CustomTypeNode(loopvartype))
        loopvar = VariableNode()
        loopvar.addChild(IdentifierNode(ctx.getChild(3).getText()))
        loopvardecl.addChild(loopvar)
        arrayAccess = ArrayAccessNode()
        arrayAccess.addChild(deepcopy(listvar))
        arrayAccess.addChild(deepcopy(indexvar))
        loopvardecl.addChild(arrayAccess)
        self.current.getChildNodes()[-1].childNodes.insert(0, loopvardecl)
        loopvardecl.parent = self.current.getChildNodes()[-1]

        #increment should be last statement of block
        increment = AssignmentNode()
        increment.addChild(deepcopy(indexvar))
        binaryExpressionNode = BinaryOperatorNode("+")
        binaryExpressionNode.addChild(deepcopy(indexvar))
        binaryExpressionNode.addChild(LiteralNode("1"))
        increment.addChild(binaryExpressionNode)
        self.current.getChildNodes()[-1].addChild(increment)

        self.current = self.current.getParent()

###############################################################################

    def enterBreakStatement(self, ctx:JavaParser.BreakStatementContext):
        """ Enter break statement """
        breakNode = BreakNode()
        breakNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        breakNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        breakNode.trace['symbol'] = "break"
        self.current.addChild(breakNode)
        self.current = breakNode

    def exitBreakStatement(self, ctx:JavaParser.BreakStatementContext):
        """ Exit break statement """
        self.current = self.current.getParent()

###############################################################################

    def enterContinueStatement(self, ctx:JavaParser.ContinueStatementContext):
        """ Enter break statement """
        continueNode = ContinueNode()
        continueNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        continueNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        continueNode.trace['symbol'] = "continue"
        self.current.addChild(continueNode)
        self.current = continueNode

    def exitContinueStatement(self, ctx:JavaParser.ContinueStatementContext):
        """ Exit break statement """
        self.current = self.current.getParent()

###############################################################################

    def enterFunctionDefinition(self, ctx:JavaParser.FunctionDefinitionContext):
        """ Enter Function Definition """
        functionDefinitionNode = FunctionDefinitionNode()
        functionDefinitionNode.trace['lineNr'] = ctx.getChild(1).getChild(0).getSymbol().line
        functionDefinitionNode.trace['charPos'] = ctx.getChild(1).getChild(0).getSymbol().column
        functionDefinitionNode.trace['symbol'] = "funcdef"
        self.current.addChild(functionDefinitionNode)
        self.current = functionDefinitionNode

    def exitFunctionDefinition(self, ctx:JavaParser.FunctionDefinitionContext):
        """ Exit Function Definition """
        self.functions[self.current.getDeclaration().getName()] = self.current.getChildNodes()[1]
        self.current = self.current.getParent()

###############################################################################

    def enterBlock(self, ctx:JavaParser.CompoundStatementsContext):
        """ Enter a code block """
        blockNode = BlockNode()
        if ctx.getChildCount() > 1:
            blockNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
            blockNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
            blockNode.trace['symbol'] = ctx.getChild(0).getText()
            blockNode.trace['lineNrStart'] = ctx.getChild(0).getSymbol().line - 1
            blockNode.trace['lineNrEnd'] = ctx.getChild(-1).getSymbol().line
        else:
            blockNode.trace = self.current.parent.trace

        self.current.addChild(blockNode)
        self.current = blockNode

    def exitBlock(self, ctx:JavaParser.CompoundStatementsContext):
        """ Exit a code block """
        self.current = self.current.getParent()

###############################################################################

    def enterLiteral(self, ctx:JavaParser.LiteralContext):
        """ Enter a literal """
        content = ctx.getText()
        if (content[-1] in "Llfd" and content[:-1].isdigit()):
            content = content[:-1]
        literalNode = LiteralNode(content)
        literalNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        literalNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        literalNode.trace['symbol'] = ctx.getText()
        self.current.addChild(literalNode)
        self.current = literalNode

    def exitLiteral(self, ctx:JavaParser.LiteralContext):
        """ Exit a literal """
        self.current = self.current.getParent()

###############################################################################

    def enterArrayInitializer(self, ctx:JavaParser.ArrayInitializerContext):
        """ Enter an array initializer """
        arrayNode = ArrayNode()
        arrayNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        arrayNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        arrayNode.trace['symbol'] = ctx.getText()
        self.current.addChild(arrayNode)
        self.current = arrayNode

    def exitArrayInitializer(self, ctx:JavaParser.ArrayInitializerContext):
        """ Exit an array initializer """
        self.current = self.current.getParent()

###############################################################################

    def enterNestedExpression(self, ctx:JavaParser.NestedExpressionContext):
        """ Enter a nested expression """
        nestedNode = NestedExpressionNode()
        nestedNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        nestedNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        nestedNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(nestedNode)
        self.current = nestedNode

    def exitNestedExpression(self, ctx:JavaParser.NestedExpressionContext):
        """ Exit a nested expression """
        if (len(self.current.childNodes) == 1): #optimalization
            parent = self.current.parent
            child = self.current.childNodes[0]
            childIndex = 0
            for index in range(0, len(parent.childNodes)):
                if id(self.current) == id(parent.childNodes[index]):
                    childIndex = index
                    break
            parent.childNodes[childIndex] = child
            child.parent = parent

        self.current = self.current.getParent()

###############################################################################

    def enterPrefixExpression(self, ctx:JavaParser.PrefixExpressionContext):
        """ Enter a prefix expression """
        operator = ctx.getChild(0).getText()
        prefixexpressionNode = UnaryOperatorNode(operator)
        self.current.addChild(prefixexpressionNode)
        self.current = prefixexpressionNode
        self.current.trace['lineNr'] = ctx.getChild(0).getChild(0).getSymbol().line
        self.current.trace['charPos'] = ctx.getChild(0).getChild(0).getSymbol().column
        self.current.trace['symbol'] = operator

    def exitPrefixExpression(self, ctx:JavaParser.PrefixExpressionContext):
        """ Exit a prefix expression """
        if isinstance(self.current.childNodes[0], LiteralNode) and self.current.operator == "+":
            self.current.parent.replaceChild(self.current, self.current.childNodes[0])
            self.current = self.current.childNodes[0]
        elif isinstance(self.current.childNodes[0], LiteralNode) and self.current.operator == "-":
            self.current.parent.replaceChild(self.current, self.current.childNodes[0])
            self.current = self.current.childNodes[0]
            self.current.content = "-" + self.current.content
        self.current = self.current.getParent()

###############################################################################

    def enterUnaryPreExpression(self, ctx:JavaParser.UnaryPreExpressionContext):
        """ Enter a prefix expression """
        tempNode = TempNode()
        tempNode.trace['lineNr'] = ctx.getChild(0).getChild(0).getSymbol().line
        tempNode.trace['charPos'] = ctx.getChild(0).getChild(0).getSymbol().column
        tempNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(tempNode)
        self.current = tempNode

    def exitUnaryPreExpression(self, ctx:JavaParser.UnaryPreExpressionContext):
        """ Exit a prefix expression """
        operator = ctx.getChild(0).getText()

        # create an x = x + 1 or x = x - 1 statement
        assignNode = AssignmentNode()
        assignNode.trace = self.current.trace
        assignNode.addChild(deepcopy(self.current.childNodes[0]))
        binaryExpressionNode = BinaryOperatorNode(operator[0])
        binaryExpressionNode.addChild(deepcopy(self.current.childNodes[0]))
        binaryExpressionNode.addChild(LiteralNode("1"))
        assignNode.addChild(binaryExpressionNode)

        # if the unary operator is a statement, replace with x = x + 1 or x = x - 1
        if isinstance(self.current.parent, BlockNode) or isinstance(self.current.parent, WhileNode):
            self.current.parent.replaceChild(self.current, assignNode)

        # if the unary operator is part of an expression, insert x = x + 1 or x = x - 1 before the current statement
        else:
            tempcurrent = self.current
            while not isinstance(tempcurrent, BlockNode):
                tempcurrent = tempcurrent.parent
            tempcurrent.childNodes.insert(-1, assignNode)
            assignNode.parent = tempcurrent
            self.current.parent.replaceChild(self.current, self.current.childNodes[0])

        self.current = self.current.parent

###############################################################################

    def enterUnaryPostExpression(self, ctx:JavaParser.UnaryPostExpressionContext):
        """ Enter a postfix expression """
        tempNode = TempNode()
        tempNode.trace['lineNr'] = ctx.getChild(1).getChild(0).getSymbol().line
        tempNode.trace['charPos'] = ctx.getChild(1).getChild(0).getSymbol().column
        tempNode.trace['symbol'] = ctx.getChild(1).getText()
        self.current.addChild(tempNode)
        self.current = tempNode

    def exitUnaryPostExpression(self, ctx:JavaParser.UnaryPostExpressionContext):
        """ Exit a postfix expression """
        operator = ctx.getChild(1).getText()

        # create an x = x + 1 or x = x - 1 statement
        assignNode = AssignmentNode()
        assignNode.trace = self.current.trace
        assignNode.addChild(deepcopy(self.current.childNodes[0]))
        binaryExpressionNode = BinaryOperatorNode(operator[0])
        binaryExpressionNode.addChild(deepcopy(self.current.childNodes[0]))
        binaryExpressionNode.addChild(LiteralNode("1"))
        assignNode.addChild(binaryExpressionNode)

        # if the unary operator is a statement, replace with x = x + 1 or x = x - 1
        if isinstance(self.current.parent, BlockNode) or isinstance(self.current.parent, WhileNode):
            self.current.parent.replaceChild(self.current, assignNode)

        # if the unary operator is part of an expression, insert x = x + 1 or x = x - 1 before the current statement
        else:
            tempcurrent = self.current
            while not isinstance(tempcurrent, BlockNode):
                tempcurrent = tempcurrent.parent
            tempcurrent.childNodes.append(assignNode)
            assignNode.parent = tempcurrent
            self.current.parent.replaceChild(self.current, self.current.childNodes[0])

        self.current = self.current.parent

###############################################################################

    def enterBinaryExpression(self, ctx:JavaParser.BinaryExpressionContext):
        """ Enter a binary expression """
        operator = ctx.getChild(1).getText()
        if (operator in ["+", "-", "*", "/", "%"]):
            binaryexpressionNode = BinaryOperatorNode(operator)
            self.current.addChild(binaryexpressionNode)
            self.current = binaryexpressionNode
        else:
            binaryexpressionNode = CompareOperatorNode(operator)
            self.current.addChild(binaryexpressionNode)
            self.current = binaryexpressionNode
        self.current.trace['lineNr'] = ctx.getChild(1).getSymbol().line
        self.current.trace['charPos'] = ctx.getChild(1).getSymbol().column
        self.current.trace['symbol'] = operator

    def exitBinaryExpression(self, ctx:JavaParser.BinaryExpressionContext):
        """ Exit a binary expression """
        self.current = self.current.getParent()

###############################################################################

    def enterArrayAccessExpression(self, ctx:JavaParser.ArrayAccessExpressionContext):
        """ Enter an array access """
        arrayNode = ArrayAccessNode()
        self.current.addChild(arrayNode)
        self.current = arrayNode
        arrayNode.trace['lineNr'] = ctx.getChild(1).getChild(0).getSymbol().line
        arrayNode.trace['charPos'] = ctx.getChild(1).getChild(0).getSymbol().column
        arrayNode.trace['symbol'] = "[ ]"

    def exitArrayAccessExpression(self, ctx:JavaParser.ArrayAccessExpressionContext):
        """ Exit an array access """
        self.current = self.current.getParent()

###############################################################################

    def enterVariable(self, ctx:JavaParser.VariableContext):
        """ Enter an variable """
        variableNode = VariableNode()
        variableNode.addChild(IdentifierNode(ctx.getText()))
        if (ctx.getChildCount() == 1):
            variableNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
            variableNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
            variableNode.trace['symbol'] = ctx.getText()
        else:
            variableNode.trace['lineNr'] = ctx.getChild(0).getChild(0).getSymbol().line
            variableNode.trace['charPos'] = ctx.getChild(0).getChild(0).getSymbol().column
            variableNode.trace['symbol'] = ctx.getText()
        self.current.addChild(variableNode)
        self.current = variableNode

    def exitVariable(self, ctx:JavaParser.VariableContext):
        """ Exit an variable """
        self.current = self.current.getParent()

###############################################################################

    def enterAssignment(self, ctx:JavaParser.AssignmentContext):
        """ Enter an assignment """
        assignNode = AssignmentNode()
        assignNode.trace['lineNr'] = ctx.getChild(1).getSymbol().line
        assignNode.trace['charPos'] = ctx.getChild(1).getSymbol().column
        assignNode.trace['symbol'] = ctx.getChild(1).getText()
        self.current.addChild(assignNode)
        self.current = assignNode

    def exitAssignment(self, ctx:JavaParser.AssignmentContext):
        """ Exit an assignment """
        self.current = self.current.getParent()

###############################################################################

    def enterAssignmentExpression(self, ctx:JavaParser.AssignmentExpressionContext):
        """ Enter an assignment """
        assignNode = AssignmentNode()
        assignNode.trace['lineNr'] = ctx.getChild(1).getSymbol().line
        assignNode.trace['charPos'] = ctx.getChild(1).getSymbol().column
        assignNode.trace['symbol'] = ctx.getChild(1).getText()
        self.current.addChild(assignNode)
        self.current = assignNode

    def exitAssignmentExpression(self, ctx:JavaParser.AssignmentExpressionContext):
        """ Exit an assignment """
        binaryExpressionNode = BinaryOperatorNode(ctx.getChild(1).getText()[0])
        binaryExpressionNode.addChild(deepcopy(self.current.getChildNodes()[0]))
        binaryExpressionNode.addChild(deepcopy(self.current.getChildNodes()[1]))
        self.current.childNodes.pop()
        self.current.addChild(binaryExpressionNode)
        self.current = self.current.getParent()

###############################################################################

    def enterFunctionCall(self, ctx:JavaParser.FunctionCallContext):
        """ Enter a function call """
        callNode = None
        callNode = FunctionCallNode()
        identifier = ctx.getChild(0).getText()
        callNode.trace['lineNr'] = ctx.getChild(1).getSymbol().line
        callNode.trace['charPos'] = (ctx.getChild(1).getSymbol().column - len(identifier))
        callNode.trace['symbol'] = identifier
        callNode.addChild(IdentifierNode(identifier))
        self.current.addChild(callNode)
        self.current = callNode

    def exitFunctionCall(self, ctx:JavaParser.FunctionCallContext):
        """ Exit a function call """
        self.current = self.current.getParent()

###############################################################################

    def enterVariableDeclarationList(self, ctx:JavaParser.VariableDeclarationListContext):
        """ Enter a variable declaration list """
        self.lastType = ""
        for i in range(0, ctx.getChildCount()):
            if (ctx.getChild(i).getText() not in ["public", "private", "protected", "static", "final"]):
                self.lastType = ctx.getChild(i).getText()
                break

    def enterVariableDeclaration(self, ctx:JavaParser.VariableDeclarationContext):
        """ Enter a variable declaration """
        variableNode = VariableDeclarationNode()
        self.current.addChild(variableNode)
        self.current = variableNode

        # Warning: what follows is an extremely fragile piece of code. DO NOT EDIT!
        # parse the declaration
        amountOfPointers = 0
        isArray = False
        arraySize = 0
        identifier = ""
        for i in range(0, ctx.getChildCount()):
            if (ctx.getChild(i).getText() == "["):
                isArray = True
                arraySize = ctx.getChild(i + 1).getText()
                if arraySize.isdigit():
                    arraySize = int(arraySize)
                else:
                    arraySize = None
                break
            elif (ctx.getChild(i).getText() == "="):
                break
            else:
                identifier = ctx.getChild(i).getText()
                variableNode.trace['lineNr'] = ctx.getChild(i).getSymbol().line
                variableNode.trace['charPos'] = ctx.getChild(i).getSymbol().column
                variableNode.trace['symbol'] = identifier

        #construct the type
        tempCurrent = variableNode
        if (isArray or '[' in self.lastType):
            arrayNode = ArrayTypeNode(arraySize)
            tempCurrent.addChild(arrayNode)
            tempCurrent = arrayNode

        if (self.lastType == "int"):
            tempCurrent.addChild(IntTypeNode())
        elif (self.lastType == "float"):
            tempCurrent.addChild(FloatTypeNode())
        elif (self.lastType == "double"):
            tempCurrent.addChild(DoubleTypeNode())
        elif (self.lastType == "char"):
            tempCurrent.addChild(CharTypeNode())
        elif (self.lastType == "void"):
            tempCurrent.addChild(VoidTypeNode())
        elif (self.lastType == "boolean"):
            tempCurrent.addChild(BoolTypeNode())
        else:
            tempCurrent.addChild(CustomTypeNode(self.lastType))

        #add the variable name
        varNode = VariableNode()
        varNode.addChild(IdentifierNode(identifier))
        variableNode.addChild(varNode)

    def exitVariableDeclaration(self, ctx:JavaParser.VariableDeclarationContext):
        """ Exit a variable declaration """
        if isinstance(self.current.childNodes[0], ArrayTypeNode) and len(self.current.childNodes) > 2 and isinstance(self.current.childNodes[2], LiteralNode):
            self.current.childNodes.pop(2) # remove the array size which was added as literal node
        elif len(self.current.childNodes) > 2 and isinstance(self.current.childNodes[2], ArrayAccessNode) and len(self.current.childNodes[2].childNodes) < 2:
            self.current.childNodes.pop(2) # remove the array access which was added
        self.current = self.current.getParent()

###############################################################################

    def enterFunctionDeclaration(self, ctx:JavaParser.FunctionDeclarationContext):
        """ Enter a function declaration """
        functionNode = FunctionDeclarationNode()
        self.current.addChild(functionNode)
        self.current = functionNode

        # find the identifier
        functionNameIndex = -1
        for i in range(0, ctx.getChildCount()):
            if (ctx.getChild(i).getText() == "("):
                functionNameIndex = i-1

        # construct the type
        typeName = ""
        for i in range(0, functionNameIndex):
            if (ctx.getChild(i).getText() not in ["public", "private", "protected", "static", "final"]):
                typeName = ctx.getChild(i).getText()

        tempCurrent = functionNode
        if ("[" in typeName):
            arrayNode = ArrayTypeNode()
            tempCurrent.addChild(arrayNode)
            tempCurrent = arrayNode
        if (typeName == "int"):
            tempCurrent.addChild(IntTypeNode())
        elif (typeName == "float"):
            tempCurrent.addChild(FloatTypeNode())
        elif (typeName == "double"):
            tempCurrent.addChild(DoubleTypeNode())
        elif (typeName == "char"):
            tempCurrent.addChild(CharTypeNode())
        elif (typeName == "void"):
            tempCurrent.addChild(VoidTypeNode())
        elif typeName != "":
            tempCurrent.addChild(CustomTypeNode(typeName))
        else:
            tempCurrent.addChild(CustomTypeNode(ctx.getChild(functionNameIndex).getText()))

        # add the function name
        identifier = ctx.getChild(functionNameIndex).getText()
        functionNode.trace['lineNr'] = ctx.getChild(functionNameIndex).getSymbol().line
        functionNode.trace['charPos'] = ctx.getChild(functionNameIndex).getSymbol().column
        functionNode.trace['symbol'] = identifier
        functionNode.addChild(IdentifierNode(identifier))

        # add the function annotations
        functionNode.annotations = self.annotations
        self.annotations = []

    def exitFunctionDeclaration(self, ctx:JavaParser.FunctionDeclarationContext):
        """ Exit a function declaration """
        if (ctx.getChild(ctx.getChildCount() - 1).getText() == "const"):
            self.current.addChild(ConstNode())
        self.current = self.current.getParent()

###############################################################################

    def enterParameter(self, ctx:JavaParser.ParameterContext):
        """ Enter a parameter for a function declaration """
        parameterNode = ParameterNode()
        self.current.addChild(parameterNode)
        self.current = parameterNode

        # find the identifier
        identifierIndex = -1
        for i in range(0, ctx.getChildCount()):
            if (ctx.getChild(i).getText() == "["):
                identifierIndex = i-1

        # construct the type
        typeName = ""
        for i in range(0, ctx.getChildCount()):
            if (ctx.getChild(i).getText() not in ["public", "private", "protected", "static", "final"]):
                typeName = ctx.getChild(i).getText()
                break

        identifier = ctx.getChild(identifierIndex).getText()
        parameterNode.trace['lineNr'] = ctx.getChild(identifierIndex).getSymbol().line
        parameterNode.trace['charPos'] = ctx.getChild(identifierIndex).getSymbol().column
        parameterNode.trace['symbol'] = identifier

        #construct the type
        tempCurrent = parameterNode
        if ("[" in typeName or identifierIndex != -1):
            arrayNode = ArrayTypeNode()
            tempCurrent.addChild(arrayNode)
            tempCurrent = arrayNode
        if (typeName == "int"):
            tempCurrent.addChild(IntTypeNode())
        elif (typeName == "float"):
            tempCurrent.addChild(FloatTypeNode())
        elif (typeName == "double"):
            tempCurrent.addChild(DoubleTypeNode())
        elif (typeName == "char"):
            tempCurrent.addChild(CharTypeNode())
        elif (typeName == "void"):
            tempCurrent.addChild(VoidTypeNode())
        elif (typeName != ""):
            tempCurrent.addChild(CustomTypeNode(typeName))
        else:
            tempCurrent.addChild(CustomTypeNode(identifier))

        #add the variable name
        parameterNode.addChild(IdentifierNode(identifier))

    def exitParameter(self, ctx:JavaParser.ParameterContext):
        """ Exit a parameter for a function declaration """
        self.current = self.current.getParent()

###############################################################################

    def enterClassDeclaration(self, ctx:JavaParser.ClassDeclarationContext):
        """ Enter class declaration """
        identifierIndex = 0
        for i in range(0, ctx.getChildCount()):
            if ctx.getChild(i).getText() == "class":
                identifierIndex = i + 1
                break
        identifier = ctx.getChild(identifierIndex).getText()
        classDeclarationNode = ClassDeclarationNode()
        classDeclarationNode.trace['lineNr'] = ctx.getChild(identifierIndex - 1).getSymbol().line
        classDeclarationNode.trace['charPos'] = ctx.getChild(identifierIndex - 1).getSymbol().column + 6
        classDeclarationNode.trace['symbol'] = identifier
        self.current.addChild(classDeclarationNode)
        self.current = classDeclarationNode
        classDeclarationNode.addChild(IdentifierNode(identifier))

    def exitClassDeclaration(self, ctx:JavaParser.ClassDeclarationContext):
        """ Exit class declaration """
        self.current = self.current.getParent()

###############################################################################

    def enterClassDefinition(self, ctx:JavaParser.ClassDefinitionContext):
        """ Enter Class Definition """
        bracketIndex = 0
        for i in range(0, ctx.getChildCount()):
            if ctx.getChild(i).getText() == "{":
                bracketIndex = i
                break
        classDefinitionNode = ClassDefinitionNode()
        classDefinitionNode.trace['lineNr'] = ctx.getChild(bracketIndex).getSymbol().line
        classDefinitionNode.trace['charPos'] = ctx.getChild(bracketIndex).getSymbol().column
        classDefinitionNode.trace['symbol'] = "{"
        self.current.addChild(classDefinitionNode)
        self.current = classDefinitionNode

    def exitClassDefinition(self, ctx:JavaParser.ClassDefinitionContext):
        """ Exit Class Definition """
        self.current = self.current.getParent()

###############################################################################

    def enterAnnotation(self, ctx:JavaParser.AnnotationContext):
        """ Enter Annotation """
        self.annotations.append(ctx.getChild(1).getText())

    def exitAnnotation(self, ctx:JavaParser.AnnotationContext):
        """ Exit Annotation"""
        pass

###############################################################################

    def enterPipedExpression(self, ctx:JavaParser.PipedExpressionContext):
        """ Enter piped expression """
        tempNode = TempNode()
        self.current.addChild(tempNode)
        self.current = tempNode

    def exitPipedExpression(self, ctx:JavaParser.PipedExpressionContext):
        """ Exit piped expression """
        firstChild = self.current.getChildNodes()[0]
        secondChild = self.current.getChildNodes()[1]

        # memberCall
        if isinstance(secondChild, FunctionCallNode):
            functionMemberCallNode = FunctionMemberCallNode()
            functionMemberCallNode.trace['lineNr'] = ctx.getChild(1).getSymbol().line
            functionMemberCallNode.trace['charPos'] = ctx.getChild(1).getSymbol().column
            functionMemberCallNode.trace['symbol'] = ctx.getText()

            functionMemberCallNode.addChild(firstChild)
            for child in secondChild.getChildNodes():
                functionMemberCallNode.addChild(child)
            self.current.parent.replaceChild(self.current, functionMemberCallNode)

        # variable
        elif isinstance(secondChild, VariableNode):
            if secondChild.getName() == "length":
                variableNode = SizeOfNode()
            else:
                variableNode = VariableNode()
            variableNode.trace['lineNr'] = ctx.getChild(2).getChild(0).getChild(0).getSymbol().line
            variableNode.trace['charPos'] = ctx.getChild(2).getChild(0).getChild(0).getSymbol().column
            variableNode.trace['symbol'] = ctx.getChild(2).getChild(0).getChild(0).getText()
            variableNode.addChild(firstChild)
            if secondChild.getName() != "length":
                for child in secondChild.getChildNodes():
                    variableNode.addChild(child)
            self.current.parent.replaceChild(self.current, variableNode)

        else:
            raise Exception("Unexpected situation occurred during parsing!")

        self.current = self.current.getParent()

###############################################################################

    def enterCastExpression(self, ctx:JavaParser.CastExpressionContext):
        """ Enter type cast expression """
        convNode = ConversionNode()
        self.current.addChild(convNode)
        self.current = convNode

    def exitCastExpression(self, ctx:JavaParser.CastExpressionContext):
        """ Exit type cast expression """
        self.current = self.current.getParent()

###############################################################################

    def enterTryStatement(self, ctx:JavaParser.TryStatementContext):
        """ Enter a try statement """
        tryNode = TryNode()
        tryNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        tryNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        tryNode.trace['symbol'] = ctx.getText()
        self.current.addChild(tryNode)
        self.current = tryNode

    def exitTryStatement(self, ctx:JavaParser.TryStatementContext):
        """ Enter a try statement """
        self.current = self.current.getParent()

###############################################################################

    def enterCatchStatement(self, ctx:JavaParser.CatchStatementContext):
        """ Enter a catch statement """
        catchNode = CatchNode()
        catchNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        catchNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        catchNode.trace['symbol'] = ctx.getText()
        self.current.addChild(catchNode)
        self.current = catchNode

    def exitCatchStatement(self, ctx:JavaParser.CatchStatementContext):
        """ Enter a catch statement """
        self.current = self.current.getParent()
