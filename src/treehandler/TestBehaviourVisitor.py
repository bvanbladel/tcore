"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from copy import deepcopy
from utils.Logger import Logger
from treehandler.Visitor import Visitor
from AST.Node import Node
from AST.AssertNode import AssertNode
from AST.IdentifierNode import IdentifierNode
from AST.LiteralNode import LiteralNode
from AST.VariableNode import VariableNode
from AST.VariableDeclarationNode import VariableDeclarationNode
from AST.BinaryOperatorNode import *
from AST.UnaryOperatorNode import UnaryOperatorNode
from AST.BlockNode import BlockNode
from AST.ReturnNode import ReturnNode
from AST.AssignmentNode import AssignmentNode
from AST.FunctionCallNode import FunctionCallNode, FunctionMemberCallNode
from AST.FunctionDefinitionNode import FunctionDefinitionNode
from AST.IfNode import IfNode
from AST.IfElseNode import IfElseNode
from AST.ArrayNode import ArrayNode
from AST.ArrayAccessNode import ArrayAccessNode
from AST.WhileNode import WhileNode
from AST.TempNode import TempNode
from AST.TryNode import TryNode
from AST.CatchNode import CatchNode
from AST.SizeOfNode import SizeOfNode
from AST.ConversionNode import ConversionNode
from AST.TestNode import TestNode
from utils.DynamicAnalysisTable import DynamicAnalysisTable
from utils.TreeConstructorHelper import TreeConstructorHelper

class TestBehaviourVisitor(Visitor):
    """ Visitor of the AST that creates the TBT
    """

    def __init__(self, filename, logger:Logger, whiteboxTestFunctions, allFunctionsDict):
        """ Constructor """
        self.logger = logger
        self.filename = filename
        self.tbt = Node()
        self.subtrees = TreeConstructorHelper()
        self.whiteboxTestFunctions = whiteboxTestFunctions
        self.allFunctionsDict = allFunctionsDict
        self.symbolTable = DynamicAnalysisTable()
        self.id_gen = 0
        self.supportedAsserts = ["assertEquals", "assertTrue", "assertFalse", "assertNull", "fail"]

    def getTBT(self):
        """ Return the constructed TBT """
        return self.tbt

    def visit(self, node:Node):
        """ Function that visits a node"""
        self.logger.debug(node.trace, "enter %s" % str(node))
        self.enterNode(node)
        self.logger.debug(node.trace, "handling %s" % str(node))
        self.exitNode(node)
        self.logger.debug(node.trace, "exit %s" % str(node))

    def enterNode(self, node:Node):
        """ Function that is called upon entering a node during visit """
        if isinstance(node, LiteralNode):
            self.handleLiteralEnter(node)
        elif isinstance(node, FunctionMemberCallNode):
            self.handleFunctionMemberCallEnter(node)
        elif isinstance(node, FunctionCallNode):
            self.handleFunctionCallEnter(node)
        elif isinstance(node, CompareOperatorNode):
            self.handleCompareOperatorEnter(node)
        elif isinstance(node, BinaryOperatorNode):
            self.handleBinaryOperatorEnter(node)
        elif isinstance(node, UnaryOperatorNode):
            self.handleUnaryOperatorEnter(node)
        elif isinstance(node, VariableNode):
            self.handleVariableEnter(node)
        elif isinstance(node, VariableDeclarationNode):
            self.handleVariableDeclarationEnter(node)
        elif isinstance(node, AssignmentNode):
            self.handleAssignmentEnter(node)
        elif isinstance(node, BlockNode):
            self.handleBlockEnter(node)
        elif isinstance(node, ReturnNode):
            self.handleReturnEnter(node)
        elif isinstance(node, IfNode):
            self.handleIfEnter(node)
        elif isinstance(node, IfElseNode):
            self.handleIfElseEnter(node)
        elif isinstance(node, ArrayNode):
            self.handleArrayEnter(node)
        elif isinstance(node, ArrayAccessNode):
            self.handleArrayAccessEnter(node)
        elif isinstance(node, WhileNode):
            self.handleWhileEnter(node)
        elif isinstance(node, TryNode):
            self.handleTryEnter(node)
        elif isinstance(node, CatchNode):
            self.handleCatchEnter(node)
        elif isinstance(node, SizeOfNode):
            self.handleSizeOfEnter(node)
        elif isinstance(node, ConversionNode):
            self.handleConversionEnter(node)
        else:
            pass

    def exitNode(self, node:Node):
        """ Function that is called upon exiting a node during visit """
        if isinstance(node, LiteralNode):
            self.handleLiteralExit(node)
        elif isinstance(node, FunctionMemberCallNode):
            self.handleFunctionMemberCallExit(node)
        elif isinstance(node, FunctionCallNode):
            self.handleFunctionCallExit(node)
        elif isinstance(node, CompareOperatorNode):
            self.handleCompareOperatorExit(node)
        elif isinstance(node, BinaryOperatorNode):
            self.handleBinaryOperatorExit(node)
        elif isinstance(node, UnaryOperatorNode):
            self.handleUnaryOperatorExit(node)
        elif isinstance(node, VariableNode):
            self.handleVariableExit(node)
        elif isinstance(node, VariableDeclarationNode):
            self.handleVariableDeclarationExit(node)
        elif isinstance(node, AssignmentNode):
            self.handleAssignmentExit(node)
        elif isinstance(node, BlockNode):
            self.handleBlockExit(node)
        elif isinstance(node, ReturnNode):
            self.handleReturnExit(node)
        elif isinstance(node, IfNode):
            self.handleIfExit(node)
        elif isinstance(node, IfElseNode):
            self.handleIfElseExit(node)
        elif isinstance(node, ArrayNode):
            self.handleArrayExit(node)
        elif isinstance(node, ArrayAccessNode):
            self.handleArrayAccessExit(node)
        elif isinstance(node, WhileNode):
            self.handleWhileExit(node)
        elif isinstance(node, TryNode):
            self.handleTryExit(node)
        elif isinstance(node, CatchNode):
            self.handleCatchExit(node)
        elif isinstance(node, SizeOfNode):
            self.handleSizeOfExit(node)
        else:
            pass

    def handleFunctionCallEnter(self, node:FunctionCallNode):
        """ Handle entering a FunctionCallNode """
        functionName = node.getName()

        # if we encounter an assert
        if (functionName in self.supportedAsserts):
            self.handleAssertEnter(node)

        # If it is a testfunction: consider white box
        elif (functionName in self.whiteboxTestFunctions):
            functionDefinition = self.allFunctionsDict[functionName].getParent()
            if (not isinstance(functionDefinition, FunctionDefinitionNode)):
                self.logger.error(node.trace, "Could not find definition for called function.")
            else:

                # open parameter scope
                self.symbolTable.openScope()

                # visit arguments of function
                for arg in node.getArguments():
                    self.subtrees.insert(TempNode())
                    arg.accept(self)

                # Link parameters with arguments
                parameters = functionDefinition.getDeclaration().getParameters()
                for i in range(len(parameters)-1,-1,-1):
                    var = self.subtrees.getLast()
                    if (isinstance(var, TempNode) and len(var.getChildNodes())):
                        var = var.getChildNodes()[0]
                    self.symbolTable.enterSymbol(parameters[i].getName(), var)
                    self.subtrees.pop()

                # Calculate the function body
                functionDefinition.getChildNodes()[1].accept(self)

                # close the parameter scope
                self.symbolTable.closeScope()

        # If it is a production call function: consider black box
        else:
            callNode = FunctionCallNode()

            # IdentifierNode of the function name
            callNode.addChild(IdentifierNode(functionName))

            # visit arguments of function
            for arg in node.getArguments():
                self.subtrees.insert(TempNode())
                arg.accept(self)
                if (len(self.subtrees.getLast().getChildNodes())):
                    callNode.addChild(self.subtrees.getLast().getChildNodes()[0])
                self.subtrees.pop()

            # Append to current
            self.subtrees.append(callNode)

    def handleFunctionCallExit(self, node:FunctionCallNode):
        """ Handle exiting a FunctionCallNode """
        if (node.getName() in self.supportedAsserts):
            self.handleAssertExit(node)
        elif (node.getName() not in self.whiteboxTestFunctions):
            self.subtrees.backtrack()

    def handleFunctionMemberCallEnter(self, node:FunctionMemberCallNode):
        """ Handle entering a FunctionMemberCallNode """
        functionName = node.getName()

        if (functionName in self.supportedAsserts):
            self.handleAssertEnter(node)

        else:
            # FunctionMemberCallNode
            callNode = FunctionMemberCallNode()
            self.subtrees.insert(callNode)

            # visit object being called
            node.getChildNodes()[0].accept(self)

            # IdentifierNode of the function name
            self.subtrees.append(IdentifierNode(node.getName()))
            self.subtrees.backtrack()

            # visit arguments of function
            for arg in node.getArguments():
                self.subtrees.insert(TempNode())
                arg.accept(self)
                if (len(self.subtrees.getLast().getChildNodes())):
                    callNode.addChild(self.subtrees.getLast().getChildNodes()[0])
                self.subtrees.pop()

    def handleFunctionMemberCallExit(self, node:FunctionMemberCallNode):
        """ Handle exiting a FunctionMemberCallNode """
        if (node.getName() in self.supportedAsserts):
            self.handleAssertExit(node)
        else:
            try:
                temp = TempNode()
                temp.addChild(deepcopy(self.subtrees.getLast()))
                self.symbolTable.updateSymbol(node.getObject().getName(), temp.childNodes[0])
            except:
                pass
            self.subtrees.append(self.subtrees.pop())
        self.subtrees.backtrack()

    def handleAssertEnter(self, node:Node):
        """ Handle entering an assert """
        assertNode = AssertNode()
        assertNode.trace = node.trace
        assertNode.trace['file'] = self.filename
        assertNode.old_parent = node.parent
        if (node.getName() == "assertTrue"):
            assertNode.addChild(LiteralNode("True"))
        elif (node.getName() == "assertFalse"):
            assertNode.addChild(LiteralNode("False"))
        elif (node.getName() == "assertNull"):
            assertNode.addChild(LiteralNode("Null"))
        elif (node.getName() == "fail"):
            assertNode.addChild(IdentifierNode("fail"))
            if isinstance(node.getParent(), BlockNode) and isinstance(node.getParent().getParent(), CatchNode):
                assertNode.addChild(deepcopy(self.symbolTable.retrieveSymbol(node.getParent().getParent().childNodes[0].getName())))
            elif isinstance(node.getParent(), BlockNode):
                assertNode.addChild(deepcopy(node.getParent()))
        self.subtrees.insert(assertNode)

        # visit arguments of function
        for arg in node.getArguments():
            arg.accept(self)

    def handleAssertExit(self, node:Node):
        """ Handle exiting an assert """
        assertTree = self.subtrees.pop()
        # check if assert is conditional
        for subtree in reversed(self.subtrees.getData()):
            # if we are inside a conditional
            if isinstance(subtree, IfNode) or isinstance(subtree, WhileNode):
                temp = deepcopy(subtree)
                temp.addChild(assertTree)
                assertTree = temp
        testNode = TestNode()
        testNode.trace = node.trace
        testNode.addChild(assertTree)
        self.tbt.addChild(testNode)

    def handleCompareOperatorEnter(self, node:CompareOperatorNode):
        """ Handle entering a CompareOperatorNode """
        compareNode = CompareOperatorNode(node.operator)
        self.subtrees.append(compareNode)
        node.getChildNodes()[0].accept(self)
        node.getChildNodes()[1].accept(self)

    def handleCompareOperatorExit(self, node:CompareOperatorNode):
        """ Handle exiting a CompareOperatorNode """
        operands = self.subtrees.getLast().getChildNodes()
        if isinstance(operands[0], LiteralNode) and isinstance(operands[1], LiteralNode) and operands[0].getValue() is not None and operands[1].getValue() is not None:
            operator = node.operator
            result = LiteralNode("NaN")
            if (operator == "=="):
                result.content = str(operands[0].getValue() == operands[1].getValue())
            elif (operator == "!="):
                result.content = str(operands[0].getValue() != operands[1].getValue())
            elif (operator == ">"):
                result.content = str(operands[0].getValue() > operands[1].getValue())
            elif (operator == ">="):
                result.content = str(operands[0].getValue() >= operands[1].getValue())
            elif (operator == "<"):
                result.content = str(operands[0].getValue() < operands[1].getValue())
            elif (operator == "<="):
                result.content = str(operands[0].getValue() <= operands[1].getValue())
            elif (operator == "&&"):
                result.content = str(operands[0].getValue() and operands[1].getValue())
            elif (operator == "||"):
                result.content = str(operands[0].getValue() or operands[1].getValue())
            else:
                pass
            self.subtrees.replace(result)

        # peephole optimizations
        elif isinstance(operands[0], LiteralNode) or isinstance(operands[1], LiteralNode):
            literalIndex = 0
            otherIndex = 1
            if isinstance(operands[1], LiteralNode):
                literalIndex = 1
                otherIndex = 0
            operator = self.subtrees.getLast().operator

            if (operator == "&&"):
                if (operands[literalIndex].getValue()):
                    self.subtrees.replace(deepcopy(operands[otherIndex]))
                else:
                    self.subtrees.replace(LiteralNode("False"))
            elif (operator == "||"):
                if (operands[literalIndex].getValue()):
                    self.subtrees.replace(LiteralNode("True"))
                else:
                    self.subtrees.replace(deepcopy(operands[otherIndex]))

        self.subtrees.backtrack()

    def handleLiteralEnter(self, node:LiteralNode):
        """ Handle entering a LiteralNode """
        literalNode = LiteralNode(node.content)
        self.subtrees.append(literalNode)

    def handleLiteralExit(self, node:LiteralNode):
        """ Handle exiting a LiteralNode """
        self.subtrees.backtrack()

    def handleVariableEnter(self, node:VariableNode):
        """ Handle entering a VariableNode """
        var = deepcopy(self.symbolTable.retrieveSymbol(node.getName()))
        if (var != None):
            self.subtrees.append(var)
        else:
            var = deepcopy(node)
            var.parent = None
            self.subtrees.append(var)
            self.logger.warning(node.trace, "Could not find declaration for used variable.")

    def handleVariableExit(self, node:VariableNode):
        """ Handle exiting a VariableNode """
        self.subtrees.backtrack()

    def handleVariableDeclarationEnter(self, node:VariableDeclarationNode):
        """ Handle entering a VariableDeclarationNode """
        self.subtrees.insert(TempNode())
        if (node.hasDefinition()):
            node.getDefinition().accept(self)
        else:
            self.logger.warning(node.trace, "Variable declared but not initialized.")

    def handleVariableDeclarationExit(self, node:VariableDeclarationNode):
        """ Handle exiting a VariableDeclarationNode """
        var = self.subtrees.getLast()
        if (isinstance(var, TempNode) and len(var.getChildNodes())):
            var = var.getChildNodes()[0]
        try:
            self.symbolTable.enterSymbol(node.getName(), var)
        except Exception as e:
            self.logger.warning(node.trace, e.args[0])
        self.subtrees.pop()

    def handleAssignmentEnter(self, node:AssignmentNode):
        """ Handle entering a AssignmentNode """
        self.subtrees.insert(TempNode())
        node.getRHS().accept(self)

    def handleAssignmentExit(self, node:AssignmentNode):
        """ Handle exiting a AssignmentNode """
        # get value
        value = self.subtrees.getLast()
        if (isinstance(value, TempNode) and len(value.getChildNodes())):
            value = value.getChildNodes()[0]

        currentvalue = self.symbolTable.retrieveSymbol(node.getLHS().getName())
        if currentvalue == None:
            currentvalue = value

        # check if value is conditional
        for subtree in self.subtrees.getData():
            # if we are inside a conditional
            if isinstance(subtree, IfNode):
                # check if we already handled this conditional
                if isinstance(currentvalue, IfNode) and currentvalue.id == subtree.id:
                    currentvalue = currentvalue.getChildNodes()[1]

                # if not: replace the current value with the new conditional
                else:
                    condvalue = deepcopy(subtree)
                    condvalue.addChild(value)
                    if currentvalue.parent is not None and not isinstance(currentvalue.parent, TempNode):
                        currentpar = currentvalue.parent
                        currentpar.replaceChild(currentvalue, condvalue)
                    condvalue.addChild(currentvalue)
                    while currentvalue.parent is not None and not isinstance(currentvalue.parent, TempNode):
                        currentvalue = currentvalue.parent
                    value = currentvalue
                    break

            # if we are at the variable
            elif subtree == self.subtrees.getLast():
                if currentvalue.parent is not None and not isinstance(currentvalue.parent, TempNode):
                    currentpar = currentvalue.parent
                    currentpar.replaceChild(currentvalue, value)
                    while currentvalue.parent is not None and not isinstance(currentvalue.parent, TempNode):
                        currentvalue = currentvalue.parent
                    value = currentvalue

        self.subtrees.pop()

        if isinstance(node.getLHS(), ArrayAccessNode) and isinstance(currentvalue, ArrayNode):
            self.subtrees.insert(TempNode())
            node.getLHS().childNodes[-1].accept(self)
            index = self.subtrees.getLast().childNodes[0].getValue()
            self.subtrees.pop()
            value.parent = currentvalue
            currentvalue.childNodes[index] = value
            value = currentvalue

        # update symbolTable
        self.symbolTable.updateSymbol(node.getLHS().getName(), value)

    def handleBinaryOperatorEnter(self, node:BinaryOperatorNode):
        """ Handle entering a BinaryOperatorNode """
        operatorNode = BinaryOperatorNode(node.operator)
        self.subtrees.append(operatorNode)
        node.getChildNodes()[0].accept(self)
        node.getChildNodes()[1].accept(self)

    def handleBinaryOperatorExit(self, node:BinaryOperatorNode):
        """ Handle exiting a BinaryOperatorNode """
        operands = self.subtrees.getLast().getChildNodes()
        if isinstance(operands[0], LiteralNode) and isinstance(operands[1], LiteralNode) and operands[0].getValue() is not None and operands[1].getValue() is not None:
            operator = node.operator
            result = LiteralNode("NaN")
            if (operator == "+"):
                result.content = str(operands[0].getValue() + operands[1].getValue())
            elif (operator == "-"):
                result.content = str(operands[0].getValue() - operands[1].getValue())
            elif (operator == "*"):
                result.content = str(operands[0].getValue() * operands[1].getValue())
            elif (operator == "/"):
                result.content = str(operands[0].getValue() / operands[1].getValue())
            else:
                pass
            self.subtrees.replace(result)
        self.subtrees.backtrack()

    def handleUnaryOperatorEnter(self, node:UnaryOperatorNode):
        """ Handle entering a UnaryOperatorNode """
        operatorNode = UnaryOperatorNode(node.operator)
        self.subtrees.append(operatorNode)
        node.getChildNodes()[0].accept(self)

    def handleUnaryOperatorExit(self, node:UnaryOperatorNode):
        """ Handle exiting a UnaryOperatorNode """
        operand = self.subtrees.getLast().getChildNodes()[0]
        if isinstance(operand, LiteralNode) and operand.getValue() is not None:
            result = LiteralNode("NaN")
            if (node.operator == "-"):
                result.content = str(- operand.getValue())
            elif (node.operator == "!"):
                result.content = str(not operand.getValue())
            else:
                result.content = str(operand.getValue())
            self.subtrees.replace(result)
        else:
            pass
        self.subtrees.backtrack()

    def handleBlockEnter(self, node:BlockNode):
        """ Handle entering a BlockNode """
        self.symbolTable.openScope()
        for statement in node.getChildNodes():
            statement.accept(self)

    def handleBlockExit(self, node:BlockNode):
        """ Handle exiting a BlockNode """
        self.symbolTable.closeScope()

    def handleConversionEnter(self, node:ConversionNode):
        """ handle entering a ConversionNode """
        node.childNodes[0].accept(self)

    def handleReturnEnter(self, node:ReturnNode):
        """ Handle entering a ReturnNode """
        node.getChildNodes()[0].accept(self)

    def handleReturnExit(self, node:ReturnNode):
        """ Handle exiting a ReturnNode """
        pass

    def handleTryEnter(self, node:TryNode):
        """ Handle entering a TryNode """
        for child in node.getChildNodes():
            child.accept(self)

    def handleTryExit(self, node:TryNode):
        """ Handle exiting a TryNode """
        pass

    def handleCatchEnter(self, node:CatchNode):
        """ Handle entering a CatchNode """
        # add variable declaration
        if isinstance(node.getChildNodes()[0], VariableDeclarationNode):
            try:
                var = deepcopy(node.getChildNodes()[0].getTypeNode())
                var.parent = None
                var.addChild(TryNode())
                var.getChildNodes()[0].addChild(deepcopy(node.parent.getChildNodes()[0]))
                self.symbolTable.enterSymbol(node.getChildNodes()[0].getName(), var)
            except Exception as e:
                self.logger.warning(node.getChildNodes()[1].trace, e.args[0])

        # enter the catch block
        node.getChildNodes()[-1].accept(self)

    def handleCatchExit(self, node:CatchNode):
        """ Handle exiting a CatchNode """
        pass

    def handleIfEnter(self, node:IfNode):
        """ Handle entering a IfNode """
        # visit condition
        self.subtrees.insert(TempNode())
        node.getChildNodes()[0].accept(self)

        try:
            value = self.subtrees.getLast().getChildNodes()[0].getValue()
            self.subtrees.pop()

            # if condition evaluated to True: visit the if-block
            if (value):
                # Calculate the if body
                node.getChildNodes()[1].accept(self)

        # the condition could not be evaluated: probably because it contains a call to production
        except AttributeError:
            # get the condition
            condition = self.subtrees.getLast().getChildNodes()[0]
            self.subtrees.pop()

            # add the ifnode to the subtrees
            ifnode = IfNode()
            ifnode.id = self.id_gen
            self.id_gen += 1
            ifnode.addChild(condition)
            self.subtrees.insert(ifnode)

            # visit the if body
            node.getChildNodes()[1].accept(self)

            # remove the ifnode from the subtrees
            self.subtrees.pop()

    def handleIfExit(self, node:IfNode):
        """ Handle exiting a IfNode """
        self.subtrees.backtrack()

    def handleIfElseEnter(self, node:IfElseNode):
        """ Handle entering a IfElseNode """
        # visit condition
        self.subtrees.insert(TempNode())
        node.getChildNodes()[0].accept(self)

        try:
            value = self.subtrees.getLast().getChildNodes()[0].getValue()
            self.subtrees.pop()

            # if condition evaluated to True: visit the if-block
            if (value):
                 # Calculate the if body
                 node.getChildNodes()[1].accept(self)

            else:
                 # Calculate the else body
                 node.getChildNodes()[2].accept(self)

        # the condition could not be evaluated: probably because it contains a call to production
        except AttributeError:
            # get the condition
            condition = self.subtrees.getLast().getChildNodes()[0]
            self.subtrees.pop()

            # add the ifnode to the subtrees
            ifnode = IfNode()
            ifnode.id = self.id_gen
            self.id_gen += 1
            ifnode.addChild(condition)
            self.subtrees.insert(ifnode)

            # visit the if body
            node.getChildNodes()[1].accept(self)

            # remove the ifnode from the subtrees
            self.subtrees.pop()

            # add the elsenode to the subtrees
            elsenode = IfNode()
            elsenode.id = self.id_gen
            self.id_gen += 1
            notnode = UnaryOperatorNode("!")
            notnode.addChild(condition)
            elsenode.addChild(notnode)
            self.subtrees.insert(elsenode)

            # visit the if body
            node.getChildNodes()[2].accept(self)

            # remove the ifnode from the subtrees
            self.subtrees.pop()


    def handleIfElseExit(self, node:IfElseNode):
        """ Handle exiting a IfElseNode """
        self.subtrees.backtrack()

    def handleArrayEnter(self, node:ArrayNode):
        """ Handle entering a ArrayNode """
        arrayNode = ArrayNode()
        self.subtrees.append(arrayNode)
        for child in node.getChildNodes():
            self.subtrees.insert(TempNode())
            child.accept(self)
            arrayNode.addChild(self.subtrees.getLast().getChildNodes()[0])
            self.subtrees.pop()

    def handleArrayExit(self, node:ArrayNode):
        """ Handle exiting a ArrayNode """
        self.subtrees.backtrack()

    def handleSizeOfEnter(self, node:SizeOfNode):
        """ Handle entering a SizeOfNode """
        # get the accessed element
        self.subtrees.insert(TempNode())
        node.getChildNodes()[0].accept(self)
        accessedElement = self.subtrees.getLast().getChildNodes()[0]
        self.subtrees.pop()

        # try to evaluate the array
        try:
            if isinstance(accessedElement, ArrayNode):
                self.subtrees.append(LiteralNode(str(len(accessedElement.getChildNodes()))))
            else:
                raise IndexError() #cannot access non-array node

        # the size of the array could not be evaluated
        except (IndexError):
            arraySizeNode = SizeOfNode()
            arraySizeNode.addChild(accessedElement)
            self.subtrees.append(arraySizeNode)

    def handleSizeOfExit(self, node:SizeOfNode):
        """ Handle exiting a SizeOfNode """
        self.subtrees.backtrack()

    def handleArrayAccessEnter(self, node:ArrayAccessNode):
        """ Handle entering an ArrayAccessNode """

        # Get the index that is being accessed
        self.subtrees.insert(TempNode())
        node.getChildNodes()[-1].accept(self)
        index = self.subtrees.getLast().getChildNodes()[0]
        self.subtrees.pop()

        if (len(node.childNodes) != 2):
            # the array access is probably multi-dimensional, we don't try to evaluate it
            arrayAccessNode = ArrayAccessNode()
            arrayAccessNode.addChild(index)
            self.subtrees.append(arrayAccessNode)

        else:
            # get the accessed element
            self.subtrees.insert(TempNode())
            node.getChildNodes()[0].accept(self)
            accessedElement = self.subtrees.getLast().getChildNodes()[0]
            self.subtrees.pop()

            # try to evaluate the array
            try:
                indexvalue = index.getValue()
                if isinstance(accessedElement, ArrayNode):
                    self.subtrees.append(accessedElement.getChildNodes()[indexvalue])
                else:
                    raise IndexError() #cannot access non-array node

            # the array access could not be evaluated: probably because it contains a call to production
            except:
                arrayAccessNode = ArrayAccessNode()
                arrayAccessNode.addChild(accessedElement)
                arrayAccessNode.addChild(index)
                self.subtrees.append(arrayAccessNode)

    def handleArrayAccessExit(self, node:ArrayAccessNode):
        """ Handle exiting an ArrayAccessNode """
        self.subtrees.backtrack()

    def handleWhileEnter(self, node:WhileNode):
        """ Handle entering a WhileNode """

        # get the condition
        self.subtrees.insert(TempNode())
        node.getChildNodes()[0].accept(self)
        condition = self.subtrees.getLast().getChildNodes()[0]
        self.subtrees.pop()

        # get the value of the condition
        try:
            value = condition.getValue()

            # while the condition evaluated to True: visit the loop-block
            while (value):

                # Calculate the loop body
                self.subtrees.insert(TempNode())
                node.getChildNodes()[1].accept(self)
                self.subtrees.pop()

                # get the condition again
                self.subtrees.insert(TempNode())
                node.getChildNodes()[0].accept(self)
                condition = self.subtrees.getLast().getChildNodes()[0]
                self.subtrees.pop()

                # evaluate the condition again
                value = condition.getValue()

        # the condition could not be evaluated: probably because it contains a call to production
        except AttributeError:

             # add the ifnode to the subtrees
             whilenode = WhileNode()
             whilenode.id = self.id_gen
             self.id_gen += 1
             whilenode.addChild(condition)
             self.subtrees.insert(whilenode)

             # visit the while body
             node.getChildNodes()[1].accept(self)

             # remove the whilenode from the subtrees
             self.subtrees.pop()

    def handleWhileExit(self, node:WhileNode):
        """ Handle exiting a WhileNode """
        self.subtrees.backtrack()
