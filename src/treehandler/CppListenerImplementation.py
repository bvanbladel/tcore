"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import re

from generated.CppListener import CppListener
from generated.CppParser import CppParser
from AST.Node import Node
from AST.IncludeNode import IncludeNode
from AST.ThrowNode import ThrowNode
from AST.TryNode import TryNode
from AST.ReturnNode import ReturnNode
from AST.ConstNode import ConstNode
from AST.IfNode import IfNode
from AST.ElseNode import ElseNode
from AST.ElseIfNode import ElseIfNode
from AST.WhileNode import WhileNode
from AST.BreakNode import BreakNode
from AST.ContinueNode import ContinueNode
from AST.FunctionDefinitionNode import FunctionDefinitionNode
from AST.BlockNode import BlockNode
from AST.VariableDeclarationNode import VariableDeclarationNode
from AST.FunctionDeclarationNode import FunctionDeclarationNode
from AST.BasicTypeNodes import *
from AST.PointerTypeNode import PointerTypeNode
from AST.ArrayTypeNode import ArrayTypeNode
from AST.CustomTypeNode import CustomTypeNode
from AST.IdentifierNode import IdentifierNode
from AST.LiteralNode import LiteralNode
from AST.ParameterNode import ParameterNode
from AST.AssignmentNode import AssignmentNode
from AST.NestedExpressionNode import NestedExpressionNode
from AST.ArrayAccessNode import ArrayAccessNode
from AST.VariableNode import VariableNode
from AST.UnaryOperatorNode import UnaryOperatorNode
from AST.BinaryOperatorNode import BinaryOperatorNode, CompareOperatorNode
from AST.FunctionCallNode import *
from AST.ClassDeclarationNode import ClassDeclarationNode
from AST.ClassDefinitionNode import ClassDefinitionNode
from AST.AccessNodes import *

class CppListenerImplementation(CppListener):
    """ Listener for Antlr Parser

    This class will generate an Abstract Syntax Tree (AST) from
    the antlr parse tree.
    """

    def __init__(self, ):
        """ Constructor """
        self.ast = None
        self.current = None
        self.inClass = False
        self.modifier = None

    def getAST(self):
        """ Return the constructed AST """
        return self.ast

###############################################################################

    def enterProgram(self, ctx:CppParser.ProgramContext):
        """ Create the root node of the AST upon entering the program """
        self.ast = Node()
        self.current = self.ast

    def exitProgram(self, ctx:CppParser.ProgramContext):
        """ Exit the program """
        self.current = None

###############################################################################

    def enterIncludeStatement(self, ctx:CppParser.IncludeStatementContext):
        """ Enter include statement """
        pass

###############################################################################

    def enterReturnStatement(self, ctx:CppParser.ReturnStatementContext):
        """ Enter return statement """
        returnNode = ReturnNode()
        returnNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        returnNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        returnNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(returnNode)
        self.current = returnNode

    def exitReturnStatement(self, ctx:CppParser.ReturnStatementContext):
        """ Exit return statement """
        self.current = self.current.getParent()

###############################################################################

    def enterIfStatement(self, ctx:CppParser.IfStatementContext):
        """ Enter if statement """
        ifNode = IfNode()
        ifNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        ifNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        ifNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(ifNode)
        self.current = ifNode

    def exitIfStatement(self, ctx:CppParser.IfStatementContext):
        """ Exit if statement """
        self.current = self.current.getParent()

    def enterElseIfStatement(self, ctx:CppParser.ElseIfStatementContext):
        """ Enter else if statement """
        elseIfNode = ElseIfNode()
        elseIfNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        elseIfNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        elseIfNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(elseIfNode)
        self.current = elseIfNode

    def exitElseIfStatement(self, ctx:CppParser.ElseIfStatementContext):
        """ Exit else if statement """
        self.current = self.current.getParent()

    def enterElseStatement(self, ctx:CppParser.ElseStatementContext):
        """ Enter else statement """
        elseNode = ElseNode()
        elseNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        elseNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        elseNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(elseNode)
        self.current = elseNode

    def exitElseStatement(self, ctx:CppParser.ElseStatementContext):
        """ Exit else statement """
        self.current = self.current.getParent()

###############################################################################

    def enterWhileStatement(self, ctx:CppParser.WhileStatementContext):
        """ Enter while statement """
        whileNode = WhileNode()
        whileNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        whileNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        whileNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(whileNode)
        self.current = whileNode

    def exitWhileStatement(self, ctx:CppParser.WhileStatementContext):
        """ EXit while statement """
        self.current = self.current.getParent()

###############################################################################

    def enterForStatement(self, ctx:CppParser.ForStatementContext):
        """ Enter for statement """
        whileNode = WhileNode()
        whileNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        whileNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        whileNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(whileNode)
        self.current = whileNode

    def exitForStatement(self, ctx:CppParser.ForStatementContext):
        """ Exit for statement """
        #init statement should be executed before the while
        init = self.current.childNodes.pop(0)
        parent = self.current.parent
        parent.childNodes.insert(0, init)
        init.parent = parent

        #increment should be last statement of block
        increment = self.current.childNodes.pop(1)
        block = self.current.childNodes[1]
        block.addChild(increment)

        self.current = self.current.getParent()

###############################################################################

    def enterBreakStatement(self, ctx:CppParser.BreakStatementContext):
        """ Enter break statement """
        breakNode = BreakNode()
        breakNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        breakNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        breakNode.trace['symbol'] = "break"
        self.current.addChild(breakNode)
        self.current = breakNode

    def exitBreakStatement(self, ctx:CppParser.BreakStatementContext):
        """ Exit break statement """
        self.current = self.current.getParent()

###############################################################################

    def enterContinueStatement(self, ctx:CppParser.ContinueStatementContext):
        """ Enter break statement """
        continueNode = ContinueNode()
        continueNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        continueNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        continueNode.trace['symbol'] = "continue"
        self.current.addChild(continueNode)
        self.current = continueNode

    def exitContinueStatement(self, ctx:CppParser.ContinueStatementContext):
        """ Exit break statement """
        self.current = self.current.getParent()

###############################################################################

    def enterFunctionDefinition(self, ctx:CppParser.FunctionDefinitionContext):
        """ Enter Function Definition """
        functionDefinitionNode = FunctionDefinitionNode()
        functionDefinitionNode.trace['lineNr'] = ctx.getChild(1).getChild(0).getSymbol().line
        functionDefinitionNode.trace['charPos'] = ctx.getChild(1).getChild(0).getSymbol().column
        functionDefinitionNode.trace['symbol'] = "funcdef"
        self.current.addChild(functionDefinitionNode)
        self.current = functionDefinitionNode
        functionDefinitionNode.member = self.inClass
        self.inClass = False

    def exitFunctionDefinition(self, ctx:CppParser.FunctionDefinitionContext):
        """ Exit Function Definition """
        self.inClass = self.current.member
        self.current = self.current.getParent()

###############################################################################

    def enterBlock(self, ctx:CppParser.CompoundStatementsContext):
        """ Enter a code block """
        blockNode = BlockNode()
        blockNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        blockNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        blockNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(blockNode)
        self.current = blockNode

    def exitBlock(self, ctx:CppParser.CompoundStatementsContext):
        """ Exit a code block """
        self.current = self.current.getParent()

###############################################################################

    def enterLiteral(self, ctx:CppParser.LiteralContext):
        """ Enter a literal """
        literalNode = LiteralNode(ctx.getText())
        literalNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        literalNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        literalNode.trace['symbol'] = ctx.getText()
        self.current.addChild(literalNode)
        self.current = literalNode

    def exitLiteral(self, ctx:CppParser.LiteralContext):
        """ Exit a literal """
        self.current = self.current.getParent()

###############################################################################

    def enterNestedExpression(self, ctx:CppParser.NestedExpressionContext):
        """ Enter a nested expression """
        nestedNode = NestedExpressionNode()
        nestedNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        nestedNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        nestedNode.trace['symbol'] = ctx.getChild(0).getText()
        self.current.addChild(nestedNode)
        self.current = nestedNode

    def exitNestedExpression(self, ctx:CppParser.NestedExpressionContext):
        """ Exit a nested expression """
        if (len(self.current.childNodes) == 1): #optimalization
            parent = self.current.parent
            child = self.current.childNodes[0]
            childIndex = 0
            for index in range(0, len(parent.childNodes)):
                if id(self.current) == id(parent.childNodes[index]):
                    childIndex = index
                    break
            parent.childNodes[childIndex] = child
            child.parent = parent

        self.current = self.current.getParent()

###############################################################################

    def enterPrefixExpression(self, ctx:CppParser.PrefixExpressionContext):
        """ Enter a prefix expression """
        operator = ctx.getChild(0).getText()
        prefixexpressionNode = UnaryOperatorNode(operator)
        self.current.addChild(prefixexpressionNode)
        self.current = prefixexpressionNode
        self.current.trace['lineNr'] = ctx.getChild(0).getChild(0).getSymbol().line
        self.current.trace['charPos'] = ctx.getChild(0).getChild(0).getSymbol().column
        self.current.trace['symbol'] = operator

    def exitPrefixExpression(self, ctx:CppParser.PrefixExpressionContext):
        """ Exit a prefix expression """
        self.current = self.current.getParent()

###############################################################################

    def enterUnaryPreExpression(self, ctx:CppParser.UnaryPreExpressionContext):
        """ Enter a prefix expression """
        operator = ctx.getChild(0).getText()
        prefixexpressionNode = UnaryOperatorNode(operator)
        self.current.addChild(prefixexpressionNode)
        self.current = prefixexpressionNode
        self.current.trace['lineNr'] = ctx.getChild(0).getChild(0).getSymbol().line
        self.current.trace['charPos'] = ctx.getChild(0).getChild(0).getSymbol().column
        self.current.trace['symbol'] = operator

    def exitUnaryPreExpression(self, ctx:CppParser.UnaryPreExpressionContext):
        """ Exit a prefix expression """
        self.current = self.current.getParent()

###############################################################################

    def enterUnaryPostExpression(self, ctx:CppParser.UnaryPostExpressionContext):
        """ Enter a postfix expression """
        operator = ctx.getChild(1).getText()
        postfixexpressionNode = UnaryOperatorNode(operator)
        postfixexpressionNode.prefix = False
        self.current.addChild(postfixexpressionNode)
        self.current = postfixexpressionNode
        self.current.trace['lineNr'] = ctx.getChild(1).getChild(0).getSymbol().line
        self.current.trace['charPos'] = ctx.getChild(1).getChild(0).getSymbol().column
        self.current.trace['symbol'] = operator

    def exitUnaryPostExpression(self, ctx:CppParser.UnaryPostExpressionContext):
        """ Exit a postfix expression """
        self.current = self.current.getParent()

###############################################################################

    def enterBinaryExpression(self, ctx:CppParser.BinaryExpressionContext):
        """ Enter a binary expression """
        operator = ctx.getChild(1).getText()
        if (operator in ["+", "-", "*", "/", "%"]):
            binaryexpressionNode = BinaryOperatorNode(operator)
            self.current.addChild(binaryexpressionNode)
            self.current = binaryexpressionNode
        else:
            binaryexpressionNode = CompareOperatorNode(operator)
            self.current.addChild(binaryexpressionNode)
            self.current = binaryexpressionNode
        self.current.trace['lineNr'] = ctx.getChild(1).getSymbol().line
        self.current.trace['charPos'] = ctx.getChild(1).getSymbol().column
        self.current.trace['symbol'] = operator

    def exitBinaryExpression(self, ctx:CppParser.BinaryExpressionContext):
        """ Exit a binary expression """
        self.current = self.current.getParent()

###############################################################################

    def enterArrayAccess(self, ctx:CppParser.ArrayAccessContext):
        """ Enter an array access """
        arrayNode = ArrayAccessNode()
        self.current.addChild(arrayNode)
        self.current = arrayNode

        identifier = ctx.getChild(0).getText()
        variableNode = VariableNode()
        variableNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        variableNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        variableNode.trace['symbol'] = identifier
        variableNode.addChild(IdentifierNode(identifier))
        arrayNode.addChild(variableNode)
        arrayNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        arrayNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        arrayNode.trace['symbol'] = identifier

    def exitArrayAccess(self, ctx:CppParser.ArrayAccessContext):
        """ Exit an array access """
        self.current = self.current.getParent()

###############################################################################

    def enterIdentifierExpression(self, ctx:CppParser.IdentifierExpressionContext):
        """ Enter an variable """
        variableNode = VariableNode()
        variableNode.addChild(IdentifierNode(ctx.getText()))
        variableNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        variableNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        variableNode.trace['symbol'] = ctx.getText()
        self.current.addChild(variableNode)
        self.current = variableNode

    def exitIdentifierExpression(self, ctx:CppParser.IdentifierExpressionContext):
        """ Exit an variable """
        self.current = self.current.getParent()

###############################################################################

    def enterVariableAssign(self, ctx:CppParser.VariableAssignContext):
        """ Enter an assignment """
        assignNode = AssignmentNode()

        if ctx.getChild(0).getText() != "*":
            assignNode.trace['lineNr'] = ctx.getChild(1).getSymbol().line
            assignNode.trace['charPos'] = ctx.getChild(1).getSymbol().column
            assignNode.trace['symbol'] = ctx.getChild(1).getText()
            self.current.addChild(assignNode)
            self.current = assignNode
            variableNode = VariableNode()
            variableNode.addChild(IdentifierNode(ctx.getChild(0).getText()))
            variableNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
            variableNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
            variableNode.trace['symbol'] = ctx.getChild(0).getText()
            assignNode.addChild(variableNode)
        else:
            assignNode.trace['lineNr'] = ctx.getChild(2).getSymbol().line
            assignNode.trace['charPos'] = ctx.getChild(2).getSymbol().column
            assignNode.trace['symbol'] = ctx.getChild(2).getText()
            self.current.addChild(assignNode)
            self.current = assignNode
            variableNode = VariableNode()
            variableNode.addChild(IdentifierNode(ctx.getChild(1).getText()))
            variableNode.trace['lineNr'] = ctx.getChild(1).getSymbol().line
            variableNode.trace['charPos'] = ctx.getChild(1).getSymbol().column
            variableNode.trace['symbol'] = ctx.getChild(1).getText()
            dereferenceNode = UnaryOperatorNode("*")
            dereferenceNode.addChild(variableNode)
            assignNode.addChild(dereferenceNode)

    def exitVariableAssign(self, ctx:CppParser.VariableAssignContext):
        """ Exit an assignment """
        self.current = self.current.getParent()

###############################################################################

    def enterArrayAssign(self, ctx:CppParser.ArrayAssignContext):
        """ Enter an assignment """
        assignNode = AssignmentNode()
        assignNode.trace['lineNr'] = ctx.getChild(1).getSymbol().line
        assignNode.trace['charPos'] = ctx.getChild(1).getSymbol().column
        assignNode.trace['symbol'] = ctx.getChild(1).getText()
        self.current.addChild(assignNode)
        self.current = assignNode

    def exitArrayAssign(self, ctx:CppParser.ArrayAssignContext):
        """ Exit an assignment """
        self.current = self.current.getParent()

###############################################################################

    def enterFunctionCall(self, ctx:CppParser.FunctionCallContext):
        """ Enter a function call """
        callNode = None
        if (ctx.getChildCount() <= 4):
            callNode = FunctionCallNode()
            identifier = ctx.getChild(0).getText()
            callNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
            callNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
            callNode.trace['symbol'] = identifier
            callNode.addChild(IdentifierNode(identifier))
        else:
            callNode = FunctionMemberCallNode()
            variableNode = VariableNode()
            variableNode.addChild(IdentifierNode(ctx.getChild(0).getText()))
            variableNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
            variableNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
            variableNode.trace['symbol'] = ctx.getChild(0).getText()
            callNode.addChild(variableNode)
            identifier = ctx.getChild(2).getText()
            callNode.trace['lineNr'] = ctx.getChild(2).getSymbol().line
            callNode.trace['charPos'] = ctx.getChild(2).getSymbol().column
            callNode.trace['symbol'] = identifier
            callNode.addChild(IdentifierNode(identifier))
        self.current.addChild(callNode)
        self.current = callNode

    def exitFunctionCall(self, ctx:CppParser.FunctionCallContext):
        """ Exit a function call """
        self.current = self.current.getParent()

###############################################################################

    def enterVariableDeclaration(self, ctx:CppParser.VariableDeclarationContext):
        """ Enter a variable declaration """
        variableNode = VariableDeclarationNode()
        self.current.addChild(variableNode)
        self.current = variableNode

        variableNode.modifier = self.modifier

        # Warning: what follows is an extremely fragile piece of code. DO NOT EDIT!
        # parse the declaration
        typeName = ctx.getChild(0).getText()
        amountOfPointers = 0
        isArray = False
        arraySize = 0
        identifier = ""
        for i in range(1, ctx.getChildCount()):
            if (ctx.getChild(i).getText() == "*"):
                amountOfPointers += 1
            elif (ctx.getChild(i).getText() == "["):
                isArray = True
                arraySize = ctx.getChild(i + 1).getText()
                if arraySize.isdigit():
                    arraySize = int(arraySize)
                else:
                    arraySize = None
                break
            elif (ctx.getChild(i).getText() == "="):
                break
            else:
                identifier = ctx.getChild(i).getText()
                variableNode.trace['lineNr'] = ctx.getChild(i).getSymbol().line
                variableNode.trace['charPos'] = ctx.getChild(i).getSymbol().column
                variableNode.trace['symbol'] = identifier

        #construct the type
        tempCurrent = variableNode
        if (isArray):
            arrayNode = ArrayTypeNode(arraySize)
            tempCurrent.addChild(arrayNode)
            tempCurrent = arrayNode
        for i in range(0, amountOfPointers):
            pointerNode = PointerTypeNode()
            tempCurrent.addChild(pointerNode)
            tempCurrent = pointerNode
        if (typeName == "int"):
            tempCurrent.addChild(IntTypeNode())
        elif (typeName == "float"):
            tempCurrent.addChild(FloatTypeNode())
        elif (typeName == "char"):
            tempCurrent.addChild(CharTypeNode())
        elif (typeName == "void"):
            tempCurrent.addChild(VoidTypeNode())
        else:
            tempCurrent.addChild(CustomTypeNode(typeName))

        #add the variable name
        varNode = VariableNode()
        varNode.addChild(IdentifierNode(identifier))
        variableNode.addChild(varNode)

    def exitVariableDeclaration(self, ctx:CppParser.VariableDeclarationContext):
        """ Exit a variable declaration """
        if isinstance(self.current.childNodes[0], ArrayTypeNode):
            self.current.childNodes.pop(2) # remove the array size which was added as literal node
        self.current = self.current.getParent()

###############################################################################

    def enterFunctionDeclaration(self, ctx:CppParser.FunctionDeclarationContext):
        """ Enter a function declaration """
        functionNode = FunctionDeclarationNode()
        self.current.addChild(functionNode)
        self.current = functionNode

        functionNode.modifier = self.modifier

        # parse the declaration
        scopeSpecifierIndex = -1
        functionNameIndex = -1
        for i in range(0, ctx.getChildCount()):
            if (ctx.getChild(i).getText() == "::"):
                scopeSpecifierIndex = i-1
            elif (ctx.getChild(i).getText() == "("):
                functionNameIndex = i-1

        index = 0
        # check for const
        if (ctx.getChild(index).getText() == "const"):
            functionNode.addChild(ConstNode())
            index += 1

        # if there is a return type, construct it
        typeName = ""
        isReference = False
        amountOfPointers = 0
        if (index != scopeSpecifierIndex and index != functionNameIndex):
            # get type name
            typeName = ctx.getChild(index).getText()
            index += 1

            # check for reference or pointer type
            if (ctx.getChild(index).getText() == '&'):
                isReference = True
                index += 1
            elif (ctx.getChild(index).getText() == '*'):
                while (ctx.getChild(index).getText() == '*'):
                    amountOfPointers += 1
                    index += 1

            #construct the type
            tempCurrent = functionNode
            if (isReference):
                dereferenceNode = UnaryOperatorNode("&")
                tempCurrent.addChild(dereferenceNode)
                tempCurrent = dereferenceNode
            for i in range(0, amountOfPointers):
                pointerNode = PointerTypeNode()
                tempCurrent.addChild(pointerNode)
                tempCurrent = pointerNode
            if (typeName == "int"):
                tempCurrent.addChild(IntTypeNode())
            elif (typeName == "float"):
                tempCurrent.addChild(FloatTypeNode())
            elif (typeName == "char"):
                tempCurrent.addChild(CharTypeNode())
            elif (typeName == "void"):
                tempCurrent.addChild(VoidTypeNode())
            else:
                tempCurrent.addChild(CustomTypeNode(typeName))

        # get identifier
        identifier = ctx.getChild(functionNameIndex).getText()
        functionNode.trace['lineNr'] = ctx.getChild(index).getSymbol().line
        functionNode.trace['charPos'] = ctx.getChild(index).getSymbol().column
        functionNode.trace['symbol'] = identifier

        #add the variable name
        functionNode.addChild(IdentifierNode(identifier))

    def exitFunctionDeclaration(self, ctx:CppParser.FunctionDeclarationContext):
        """ Exit a function declaration """
        if (ctx.getChild(ctx.getChildCount() - 1).getText() == "const"):
            self.current.addChild(ConstNode())
        self.current = self.current.getParent()

###############################################################################

    def enterParameter(self, ctx:CppParser.ParameterContext):
        """ Enter a parameter for a function declaration """
        parameterNode = ParameterNode()
        self.current.addChild(parameterNode)
        self.current = parameterNode

        index = 0

        # check for const
        if (ctx.getChild(index).getText() == "const"):
            parameterNode.addChild(ConstNode())
            index += 1

        # get type name
        typeName = ctx.getChild(index).getText()
        index += 1

        # check for reference or pointer type
        isReference = False
        amountOfPointers = 0
        if (ctx.getChild(index).getText() == '&'):
            isReference = True
            index += 1
        elif (ctx.getChild(index).getText() == '*'):
            while (ctx.getChild(index).getText() == '*'):
                amountOfPointers += 1
                index += 1

        # get identifier
        identifier = ctx.getChild(index).getText()
        parameterNode.trace['lineNr'] = ctx.getChild(index).getSymbol().line
        parameterNode.trace['charPos'] = ctx.getChild(index).getSymbol().column
        parameterNode.trace['symbol'] = identifier
        index += 1

        # check for array
        isArray = False
        if (index < ctx.getChildCount() and ctx.getChild(index).getText() == '['):
            isArray = True

        #construct the type
        tempCurrent = parameterNode
        if (isReference):
            dereferenceNode = UnaryOperatorNode("&")
            tempCurrent.addChild(dereferenceNode)
            tempCurrent = dereferenceNode
        if (isArray):
            arrayNode = ArrayTypeNode()
            tempCurrent.addChild(arrayNode)
            tempCurrent = arrayNode
        for i in range(0, amountOfPointers):
            pointerNode = PointerTypeNode()
            tempCurrent.addChild(pointerNode)
            tempCurrent = pointerNode
        if (typeName == "int"):
            tempCurrent.addChild(IntTypeNode())
        elif (typeName == "float"):
            tempCurrent.addChild(FloatTypeNode())
        elif (typeName == "char"):
            tempCurrent.addChild(CharTypeNode())
        elif (typeName == "void"):
            tempCurrent.addChild(VoidTypeNode())
        else:
            tempCurrent.addChild(CustomTypeNode(typeName))

        #add the variable name
        parameterNode.addChild(IdentifierNode(identifier))

    def exitParameter(self, ctx:CppParser.ParameterContext):
        """ Exit a parameter for a function declaration """
        self.current = self.current.getParent()

###############################################################################

    def enterClassDeclaration(self, ctx:CppParser.ClassDeclarationContext):
        identifier = ctx.getChild(1).getText()
        classDeclarationNode = ClassDeclarationNode()
        classDeclarationNode.trace['lineNr'] = ctx.getChild(1).getSymbol().line
        classDeclarationNode.trace['charPos'] = ctx.getChild(1).getSymbol().column
        classDeclarationNode.trace['symbol'] = identifier
        self.current.addChild(classDeclarationNode)
        self.current = classDeclarationNode
        classDeclarationNode.addChild(IdentifierNode(identifier))

    def exitClassDeclaration(self, ctx:CppParser.ClassDeclarationContext):
        self.current = self.current.getParent()

###############################################################################

    def enterClassDefinition(self, ctx:CppParser.ClassDefinitionContext):
        """ Enter Function Definition """
        classDefinitionNode = ClassDefinitionNode()
        classDefinitionNode.trace['lineNr'] = ctx.getChild(0).getChild(0).getSymbol().line
        classDefinitionNode.trace['charPos'] = ctx.getChild(0).getChild(0).getSymbol().column
        classDefinitionNode.trace['symbol'] = ctx.getChild(0).getChild(0).getText()
        self.current.addChild(classDefinitionNode)
        self.current = classDefinitionNode
        self.inClass = True

    def exitClassDefinition(self, ctx:CppParser.ClassDefinitionContext):
        """ Exit Function Definition """
        self.current = self.current.getParent()
        self.inClass = False

    def enterPublicAccess(self, ctx:CppParser.PublicAccessContext):
        self.modifier = "public"

    def enterProtectedAccess(self, ctx:CppParser.ProtectedAccessContext):
        self.modifier = "protected"

    def enterPrivateAccess(self, ctx:CppParser.PrivateAccessContext):
        self.modifier = "private"

###############################################################################

    def enterThrowStatement(self, ctx:CppParser.ThrowStatementContext):
        throwNode = ThrowNode()
        throwNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        throwNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        throwNode.trace['symbol'] = ctx.getText()
        self.current.addChild(throwNode)
        self.current = throwNode

    def exitThrowStatement(self, ctx:CppParser.ThrowStatementContext):
        self.current = self.current.parent

###############################################################################

    def enterTryStatement(self, ctx:CppParser.TryStatementContext):
        tryNode = TryNode()
        tryNode.trace['lineNr'] = ctx.getChild(0).getSymbol().line
        tryNode.trace['charPos'] = ctx.getChild(0).getSymbol().column
        tryNode.trace['symbol'] = ctx.getText()
        self.current.addChild(tryNode)
        self.current = tryNode

    def exitTryStatement(self, ctx:CppParser.TryStatementContext):
        self.current = self.current.parent
