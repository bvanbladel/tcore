"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from treehandler.Visitor import Visitor
from AST.Node import Node
from AST.LiteralNode import LiteralNode
from AST.IdentifierNode import IdentifierNode
from AST.AssertNode import AssertNode
from AST.TestNode import TestNode
from AST.BinaryOperatorNode import BinaryOperatorNode

class XMLGenerationVisitor(Visitor):
    """ Visitor for AST

    This class will generate an XML file representing a tree-structure
    """

    def __init__(self, filename):
        self.outputfile = open(filename+".xml",'w')
        self.nestingDepth = 0

    def closeFile(self):
        self.outputfile.close()

    def visit(self, node:Node):
        """ Function that visits a node"""
        self.outputfile.write(self.nodeToOpenXML(node))
        self.nestingDepth += 1
        for child in node.getChildNodes():
            child.accept(self)
        self.nestingDepth -= 1
        self.outputfile.write(self.nodeToCloseXML(node))

    def nodeToOpenXML(self, node:Node):
        """ Generate the open XML element for the node. """
        if isinstance(node, LiteralNode):
            return ("\t"*self.nestingDepth) + ("<LITERAL> %s " % str(node))
        elif isinstance(node, IdentifierNode):
            return ("\t"*self.nestingDepth) + ("<IDENTIFIER> %s " % str(node))
        elif isinstance(node, BinaryOperatorNode):
            return ("\t"*self.nestingDepth) + ("<OPERATION%s>\n" % str(node))
        elif isinstance(node, TestNode):
            return ("\t"*self.nestingDepth) + ("<%s file=\"%s\" trace=\"%d:%d\">\n" % (str(node).upper(), node.trace['file'], node.trace['lineNr'], node.trace['charPos']))
        elif not len(node.getChildNodes()):
            return ("\t"*self.nestingDepth) + ("<%s />\n" % str(node).upper())
        else:
            return ("\t"*self.nestingDepth) + ("<%s>\n" % str(node).upper())

    def nodeToCloseXML(self, node:Node):
        """ Generate the close XML element for the node. """
        if isinstance(node, LiteralNode):
            return "</LITERAL>\n"
        elif isinstance(node, IdentifierNode):
            return "</IDENTIFIER>\n"
        elif isinstance(node, BinaryOperatorNode):
            return ("\t"*self.nestingDepth) + ("</OPERATION%s>\n" % str(node))
        elif not len(node.getChildNodes()):
                return ""
        else:
            return ("\t"*self.nestingDepth) + ("</%s>\n" % str(node).upper())
