"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from treehandler.Visitor import Visitor
from AST.Node import Node

class DotGenerationVisitor(Visitor):
    """ Visitor for AST

    This class will generate a .dot file representing a tree-structure.
    """

    def __init__(self, filename):
        self.outputfile = open(filename+".dot",'w')
        self.outputfile.write("digraph ast {\n")
        self.ids = {}
        self.c = 0

    def closeFile(self):
        self.outputfile.write("}")
        self.outputfile.close()

    def visit(self, node:Node):
        """ Function that visits a node"""
        self.outputfile.write('"%s"[label="%s"];\n' % (self.nodeID(node), str(node).replace('"','\\"')))
        for child in node.getChildNodes():
            self.outputfile.write('\t"%s" -> "%s";\n' % (self.nodeID(node), self.nodeID(child)))
            child.accept(self)

    def nodeID(self, node:Node):
        """ Generates a node ID based on trace information """
        node_id = id(node)
        if node_id in self.ids:
            return self.ids[node_id]
        else:
            self.c += 1;
            self.ids[node_id] = self.c
            return self.c
