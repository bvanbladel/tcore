'''
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import os
from flask import Flask, flash, session, request, redirect, url_for, render_template, Markup
from werkzeug.utils import secure_filename
from Parser import Parser
from treehandler.DotGenerationVisitor import DotGenerationVisitor
from treehandler.TestBehaviourVisitor import TestBehaviourVisitor
from treehandler.PreprocessingVisitor import PreprocessingVisitor
from treehandler.TBTComparer import TBTComparer

UPLOAD_FOLDER = 'web/uploads'
TEMPLATE_FOLDER = '../web/templates'
ALLOWED_EXTENSIONS = set(['java'])

app = Flask(__name__, template_folder=TEMPLATE_FOLDER)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = 'yCEjb.D-UT?*mv#y!6sJ^b~u6cf{(EL^)x$DTmE#]2:FochdDPD]MiOYvKCqd'

treedict = {}
functionsdict = {}
parser = Parser()

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def parseAST(filename):
    try:
        parser.parseFile(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    except Exception as exception:
        flash("ERROR - Error while parsing the input file.")
        return
    ast = parser.getAST()
    preprocessingVisitor = PreprocessingVisitor()
    ast.accept(preprocessingVisitor)
    treedict[filename + "AST"] = ast
    functionsdict[filename] = parser.getFunctionsDict()


def parseTBT(filename, functions):
    tests = []
    whiteboxTestFunctions = []
    for function in functions:
        if "Test" in functionsdict[filename][function].getParent().getDeclaration().getAnnotations():
            tests.append(function)
        else:
            whiteboxTestFunctions.append(function)

    tbtgenvisitor = TestBehaviourVisitor(filename, parser.getLogger(), whiteboxTestFunctions, functionsdict[filename])
    for testcase in tests:
        functionsdict[filename][testcase].accept(tbtgenvisitor)
    treedict[filename + "TBT"] = tbtgenvisitor.getTBT()


@app.route('/', methods=['GET', 'POST'])
def tcoremain():
    if request.method == 'POST':
        # check if the before file was uploaded
        if 'beforeinputfile' in request.files:
            beforeinputfile = request.files['beforeinputfile']
            if beforeinputfile.filename == '':
                flash('Warning - No selected file. Please select a file before submitting.')
                return redirect('/')
            if beforeinputfile and allowed_file(beforeinputfile.filename):
                filename = secure_filename(beforeinputfile.filename)
                beforeinputfile.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                session['beforeinputfile'] = filename
                parseAST(filename)
                session['beforetests'] = list(functionsdict[filename].keys())
                session['beforetestsselected'] = 0
                return redirect('/')

        # check if the before file was dismissed
        elif 'beforeinputfiledismiss' in request.form:
            session.pop('beforeinputfile', None)
            session.pop('beforetestsselected', None)
            session.pop('beforetests', None)
            session.pop('compareresult', None)
            return redirect('/')

        # check if the before tests where selected
        elif 'beforetests' in request.form:
            selectedtests = []
            for test in session['beforetests']:
                if ('before' + test) in request.form:
                    selectedtests.append(test)
            parseTBT(session['beforeinputfile'], selectedtests)
            session['beforetestsselected'] = 1
            session['beforetests'] = selectedtests
            return redirect('/')

        # check if the after file was uploaded
        elif 'afterinputfile' in request.files:
            afterinputfile = request.files['afterinputfile']
            if afterinputfile.filename == '':
                flash('Warning - No selected file. Please select a file before submitting.')
                return redirect(request.url)
            if afterinputfile and allowed_file(afterinputfile.filename):
                filename = secure_filename(afterinputfile.filename)
                afterinputfile.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                session['afterinputfile'] = filename
                parseAST(filename)
                session['aftertests'] = list(functionsdict[filename].keys())
                session['aftertestsselected'] = 0
                return redirect('/')

        # check if the after file was dismissed
        elif 'afterinputfiledismiss' in request.form:
            session.pop('afterinputfile', None)
            session.pop('aftertestsselected', None)
            session.pop('aftertests', None)
            session.pop('compareresult', None)
            return redirect('/')

        # check if the after tests where selected
        elif 'aftertests' in request.form:
            selectedtests = []
            for test in session['aftertests']:
                if ('after' + test) in request.form:
                    selectedtests.append(test)
            parseTBT(session['afterinputfile'], selectedtests)
            session['aftertestsselected'] = 1
            session['aftertests'] = selectedtests
            return redirect('/')

        # check if the compare button was pressed
        elif 'compare' in request.form:
            comparer = TBTComparer(treedict[session['beforeinputfile'] + "TBT"], treedict[session['afterinputfile'] + "TBT"])
            comparer.compare()
            session['compareresult'] = comparer.getStatistics()
            session['comparereport'] = Markup(comparer.getHTMLReport(UPLOAD_FOLDER + '/' + session['beforeinputfile'], UPLOAD_FOLDER + '/' + session['afterinputfile']))
            return redirect('/')

        elif 'reset' in request.form:
            session.clear()
            return redirect('/')

        # something went wrong
        else:
            flash('ERROR - Something went wrong.')
            return redirect('/')

    return render_template('tcoremain.html')
