"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class Logger():
    """ Logger that will generate the error messages upon syntax and semantic errors.

	The error messages are inspired by the 'clang' compiler.
    No color version: used for windows and tests.
    """

    def __init__(self, inputfile:str):
        self.amountOfErrors = 0
        self.inputfile = inputfile
        self._verbose = False
        self._debug = False

    def setVerbose(self, v):
        """ Set the verbose level """
        self._verbose = v

    def setDebug(self, d):
        """ Set the debug level """
        self._debug = d

    def getErrorLine(self, lineNr):
        """ Return the line that caused the error """
        with open(self.inputfile) as f:
            for i, line in enumerate(f):
                if (i == (lineNr - 1)):
                    return line.replace("\t"," ")

    def error(self, trace, msg):
        """ error handler """
        self.amountOfErrors += 1
        errorline = self.getErrorLine(trace["lineNr"])
        print("%s:%d:%d: " % (self.inputfile, trace["lineNr"], trace["charPos"])
            + "error: "
            + msg)
        try:
            print(errorline[:-1])
            print(" " * (trace["charPos"]) + "^" + "~"*(len(trace["symbol"]) - 1))
            print("")
        except:
            print("")

    def warning(self, trace, msg):
        """ warning handler """
        errorline = self.getErrorLine(trace["lineNr"])
        print("%s:%d:%d: " % (self.inputfile, trace["lineNr"], trace["charPos"])
            + "warning: "
            + msg)
        try:
            print(errorline[:-1])
            print(" " * (trace["charPos"]) + "^" + "~"*(len(trace["symbol"]) - 1))
            print("")
        except:
            print("")


    def note(self, trace, msg):
        """ note handler """

        errorline = self.getErrorLine(trace["lineNr"])
        print("%s:%d:%d: " % (self.inputfile, trace["lineNr"], trace["charPos"])
            + "note: "
            + msg)
        try:
            print(errorline[:-1])
            print(" " * (trace["charPos"]) + "^" + "~"*(len(trace["symbol"]) - 1))
            print("")
        except:
            print("")

    def info(self, trace, msg):
        """ info handler """
        if (self._verbose):
            print("%s:%d:%d: " % (self.inputfile, trace["lineNr"], trace["charPos"])
                + "info: "
                + msg)

    def debug(self, trace, msg):
        """ debug handler """
        if (self._debug):
            print("%s:%d:%d: " % (self.inputfile, trace["lineNr"], trace["charPos"])
                + "debug: "
                + msg)
