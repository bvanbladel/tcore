"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from AST.Node import Node

class TreeConstructorHelper:
    """ Helper for the construction of the TBT.
    """

    def __init__(self):
        """ Constructor """
        self.data = []

    def __str__(self):
        """ String representation """
        return str(self.data)

    def getData(self):
        """ Return the raw data """
        return self.data

    def insert(self, node:Node):
        """ Insert a node """
        self.data.append(node)

    def append(self, node:Node):
        """ Append a node"""
        if (self.data != []):
            self.data[-1].addChild(node)
            self.data[-1] = node

    def backtrack(self):
        """ Backtrack to parent node """
        if (self.data != [] and self.data[-1].getParent() != None):
            self.data[-1] = self.data[-1].getParent()

    def replace(self, node:Node):
        """ Replacing a node with its parent node """
        if (self.data != []):
            self.data[-1].getParent().replaceChild(self.data[-1], node)
            self.data[-1] = node

    def pop(self):
        """ Pop a node """
        if (self.data != []):
           return self.data.pop()

    def getLast(self):
        """ Returns the last node """
        return self.data[-1]
