"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from AST.Node import Node

class DynamicAnalysisTable:
    """ Special Symbol Table used for Dynamic Analysis.

    Used to save variable sub-trees during construction of the TBT.
    """

    GLOBAL_SCOPE = 0

    def __init__(self):
        """ Constructor """
        self.scope = DynamicAnalysisTable.GLOBAL_SCOPE
        self.table = [] # Symbol table representation: list of dicts, index is scope
        self.table.append({})

    def __str__(self):
        """ Human readable representation """
        r = "SymbolTable {\n"
        for scope in self.table:
            r += "--- scope ---\n"
            for entry in scope:
                try:
                    r += "    %s --> %s\n" % (str(entry), str(scope[entry].getValue()))
                except:
                    r += "    %s --> %s\n" % (str(entry), str(scope[entry]))
        r += "}\n"
        return r

    def openScope(self):
        """ Open a scope """
        self.scope += 1
        self.table.append({})

    def closeScope(self):
        """ Close a scope """
        assert self.scope > DynamicAnalysisTable.GLOBAL_SCOPE
        self.scope -= 1
        self.table.pop()

    def enterSymbol(self, name:str, value:Node):
        """ Enter a symbol for the current scope """
        if(self.existsInCurrentScope(name)):
            self.table[self.scope][name] = value
            raise Exception("Redefinition of variable " + name)
        else:
            self.table[self.scope][name] = value

    def updateSymbol(self, name:str, value:Node):
        """ Update the value of a symbol """
        for s in range(self.scope, -1, -1):
            if name in self.table[s]:
                self.table[s][name] = value

    def existsInCurrentScope(self, name:str):
        """ Check if a symbol is declared in current scope"""
        return (name in self.table[self.scope])

    def retrieveSymbol(self, name:str):
        """ Get the entry for the symbol """
        for s in range(self.scope, -1, -1):
            if name in self.table[s]:
                return self.table[s][name]
        return None
