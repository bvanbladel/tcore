'''
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import sys
from Parser import Parser
from UI import UI
from treehandler.DotGenerationVisitor import DotGenerationVisitor
from treehandler.XMLGenerationVisitor import XMLGenerationVisitor
from treehandler.TestBehaviourVisitor import TestBehaviourVisitor
from treehandler.PreprocessingVisitor import PreprocessingVisitor

def writeToFile(node, outtype, filename):
    """ Write tree to file """
    if (outtype in ["dot", "DOT", "Dot"] ):
        dotgenvisitor = DotGenerationVisitor(filename)
        node.accept(dotgenvisitor)
        dotgenvisitor.closeFile()
    elif (outtype in ["xml", "XML", "Xml"]):
        xmlgenvisitor = XMLGenerationVisitor(filename)
        node.accept(xmlgenvisitor)
        xmlgenvisitor.closeFile()
    else:
        print("Error: invalid filetype given.")


def main(argv):
    """ The main function """
    args = UI();

    # Parse the source code
    parser = Parser()
    if (args.getDebug()):
        parser.parseFile(args.getInputFile())
    else:
        try:
            parser.parseFile(args.getInputFile())
        except Exception as exception:
            print("Parsing Error: " + str(exception))
            return 1
    ast = parser.getAST()
    allFunctionsDict = parser.getFunctionsDict()

    # Get the tests: default = all
    tests = args.getTests()
    if tests is None:
        if args.getVerbose():
            print("No tests specified, using default: ALL")
        tests = []
        for key in allFunctionsDict.keys():
            # junit4
            if ("Test" in allFunctionsDict[key].getParent().getDeclaration().getAnnotations()):
                tests.append(key)
            # junit3
            elif ("test" == allFunctionsDict[key].getParent().getDeclaration().getName()[:4].lower() and "testcase" in allFunctionsDict[key].getParent().getParent().getDeclaration().getName().lower()):
                tests.append(key)

    # Get the test functions: default = none
    whiteboxTestFunctions = args.getTestFunctions()
    if whiteboxTestFunctions is None:
        if args.getVerbose():
            print("No test functions specified, using default: ALL")
        whiteboxTestFunctions = []
        for key in allFunctionsDict.keys():
            if ("Test" not in allFunctionsDict[key].getParent().getDeclaration().getAnnotations()):
                whiteboxTestFunctions.append(key)

    # Write the AST
    if (args.getASTGeneration()):
        writeToFile(ast, args.getOutput(), "AST")

    preprocessingVisitor = PreprocessingVisitor()
    ast.accept(preprocessingVisitor)

    # Write the AST to Dot after preprocessing (should only be enabled when performing manual tests on  the preprocessor)
    # writeToFile(ast, args.getOutput(), "AST2")

    # Generate the Test Behaviour Tree
    logger = parser.getLogger()
    logger.setVerbose(args.getVerbose())
    logger.setDebug(args.getDebug())
    tbtgenvisitor = TestBehaviourVisitor(args.getInputFile(), logger, whiteboxTestFunctions, allFunctionsDict)
    for testcase in tests:
        allFunctionsDict[testcase].accept(tbtgenvisitor)
    tbt = tbtgenvisitor.getTBT()

    # Write the TBT
    if (args.getVerbose()):
        print("Finished Behaviour Analysis!")
        print("Writing TBT to file... (size of %d nodes)" % tbt.getSubtreeSize())
    writeToFile(tbt, args.getOutput(), args.getOutputName())

if (main(sys.argv)):
    sys.exit(1)
