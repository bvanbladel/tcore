'''
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
from antlr4 import *
from generated.CppLexer import CppLexer
from generated.CppParser import CppParser
from generated.JavaLexer import JavaLexer
from generated.JavaParser import JavaParser
from treehandler.CppListenerImplementation import CppListenerImplementation
from treehandler.JavaListenerImplementation import JavaListenerImplementation
from treehandler.CppErrorListener import CppErrorListener
from treehandler.JavaErrorListener import JavaErrorListener
from utils.Logger import Logger


class Parser():
    """ Class that will handle the parsing of source code.
    """

    def __init__(self):
        """ Constructor """
        self.ast = None
        self.allFunctionsDict = None
        self.logger = None

    def getAST(self):
        """ Return the root node of the constructed AST"""
        return self.ast

    def getFunctionsDict(self):
        """ Return the dict containing all functions {function_name -> function_declaration_node}"""
        return self.allFunctionsDict

    def getLogger(self):
        """ Return the logger"""
        return self.logger

    def parseFile(self, inputfile:str):
        """ Parse a file with the correct ANTLR parser"""

        self.logger = Logger(inputfile)
        inputFile = FileStream(inputfile)
        parser = None

        # Detect language
        if (inputfile[-3:] == "cpp"):
            lexer = CppLexer(inputFile)
            stream = CommonTokenStream(lexer)
            parser = CppParser(stream)
            parser.removeErrorListeners()
            errorListener = CppErrorListener(self.logger)
            parser.addErrorListener(errorListener)

        elif (inputfile[-4:] == "java"):
            lexer = JavaLexer(inputFile)
            stream = CommonTokenStream(lexer)
            parser = JavaParser(stream)
            parser.removeErrorListeners()
            errorListener = JavaErrorListener(self.logger)
            parser.addErrorListener(errorListener)

        else:
            raise Exception("Unsupported language")

        # Parse Java code
        tree = parser.program()
        if (self.logger.amountOfErrors):
            print("%d errors generated." % self.logger.amountOfErrors)
            raise Exception("Parsing Error")

        listener = JavaListenerImplementation()
        walker = ParseTreeWalker()
        walker.walk(listener, tree)
        self.ast = listener.getAST()
        self.allFunctionsDict = listener.getFunctions()
