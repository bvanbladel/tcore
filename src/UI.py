'''
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from argparse import ArgumentParser

class UI():
    """ Class that will handle the user interaction.
    """

    def __init__(self):
        """ Constructor """
        parser = ArgumentParser()
        parser.add_argument("-a" , "--ast", action="store_true", dest="astgen", help="Generate the AST")
        parser.add_argument("-v" , "--verbose", action="store_true", dest="verbose", help="Produce verbose output")
        parser.add_argument("-d" , "--debug", action="store_true", dest="debug", help="Produce debug output")
        parser.add_argument("-o" , "--output", type=str, dest="output", help="The output format (dot or xml)", default="dot")
        parser.add_argument("-n", "--name", type=str, dest="outputname", help="The name of the output file.", default="TBT")
        parser.add_argument("-i", "--input", type=str, dest="testfile", help="The input file")
        parser.add_argument("-t" ,"--tests", type=str, dest="tests", help="Name of the tests", nargs="+")
        parser.add_argument("-f" ,"--functions", type=str, dest="functions", help="Name of the test functions", nargs="+")
        self.args = parser.parse_args()

    def getInputFile(self):
        """ Returns the input file """
        return self.args.testfile

    def getTests(self):
        """ Returns the tests """
        return self.args.tests

    def getTestFunctions(self):
        """ Returns the test functions """
        return self.args.functions

    def getASTGeneration(self):
        """ Returns whether or not the AST dot should be generated """
        return self.args.astgen

    def getVerbose(self):
        """ Returns whether or not verbose output should be generated """
        return self.args.verbose

    def getDebug(self):
        """ Returns whether or not debug output should be generated """
        return self.args.debug

    def getOutput(self):
        """ Returns the output format """
        return self.args.output

    def getOutputName(self):
        """ Returns the output name """
        return self.args.outputname
