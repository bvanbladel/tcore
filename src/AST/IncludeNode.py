"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from AST.Node import Node

class IncludeNode(Node):
    """ Node for the include statement
    """

    def __init__(self, include:str):
        """ Constructor """
        super().__init__()
        self.include = include

    def __str__(self):
        """ String representation """
        return "Include " + self.include

    def __eq__(self, other):
        """ Equal """
        return isinstance(self, type(other)) and (self.childNodes == other.childNodes) and (self.include == other.include)
