"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import re

from AST.Node import Node
from AST.FunctionCallNode import FunctionCallNode
from AST.BasicTypeNodes import *
from AST.ArrayTypeNode import ArrayTypeNode
from AST.PointerTypeNode import PointerTypeNode
from AST.ClassDefinitionNode import ClassDefinitionNode
from AST.ParameterNode import ParameterNode
from AST.ConstNode import ConstNode

class FunctionDeclarationNode(Node):
    """ Node for a function declaration
    """

    def __init__(self):
        """ Constructor """
        super().__init__()
        self.annotations = []

    def getName(self):
        """ Return the name of the function (includes parameter types) """
        return str(self.childNodes[1])

    def getType(self):
        """ Return string representation of the full function type """
        return self.childNodes[0].getType() + "(" + ",".join([param.getType() for param in self.getParameters()]) + ")"

    def isConst(self):
        """ Return True if the method called was declared const """
        return isinstance(self.getChildNodes()[-1], ConstNode )

    def getTypeNode(self):
        """ Return the type nodes of the return type """
        return self.childNodes[0]

    def getParameters(self):
        """ Return a list of parameters """
        return [child for child in self.childNodes if isinstance(child, ParameterNode)]

    def getConflictingParameters(self):
        """ Return a list of parameters with the same name; used for semantic error """
        paramNames = []
        errorParamNames = []
        for parameter in self.getParameters():
            pname = parameter.getName()
            if pname in paramNames:
                errorParamNames.append(pname)
            else:
                paramNames.append(pname)

        returnList = []
        for parameterName in errorParamNames:
            returnList.append([])
            for parameter in self.getParameters():
                if (parameter.getName() == parameterName):
                    returnList[-1].append(parameter)

        return returnList

    def checkArgumentLengthToFew(self, node:FunctionCallNode):
        """ Return true if the function call has to few arguments """
        return len(self.getParameters()) > len(node.getArguments())

    def checkArgumentLengthToMany(self, node:FunctionCallNode):
        """ Return true if the function call has to many arguments """
        return len(self.getParameters()) < len(node.getArguments())

    def getArgumentTypes(self):
        """ Return the type nodes of the parameters """
        params = self.getParameters()
        types = []
        for param in params:
            types.append(param.childNodes[0])
        return types

    def getScopeName(self):
        """ Return the name of the class in which this function is declared, if any """
        n = self.parent
        while(not isinstance(n, ClassDefinitionNode)):
            if (n.parent == None):
                return ""
            n = n.parent
        return n.getDeclaration().getName()

    def getAnnotations(self):
        """ Return the annotations of the class declaration """
        return self.annotations

class IOFunctionDeclarationNode(FunctionDeclarationNode):
    """ Node for an IO function declaration (e.g. scanf or printf)

    This class overrides FunctionDeclarationNode in order to allow
    a variable amount of arguments for the printf and scanf functions.
    """

    def __init__(self):
        """ Constructor """
        super().__init__()
        string = ArrayTypeNode(0)
        string.addChild(CharTypeNode())
        self.flagmap = {'d': IntTypeNode(), 'f': FloatTypeNode(), 'c': CharTypeNode(), 's': string}

    def checkArgumentLengthToFew(self, node:FunctionCallNode):
        """ Return true if the function call has to few arguments """
        args = node.getArguments()
        if len(args) == 0:
            return True
        if args[0].exprtype != self.flagmap['s']:
            return False
        formatstring = args[0].content
        count = len(re.findall('\%[0-9]*[dfcs]', formatstring))
        return (count > (len(args) - 1))

    def checkArgumentLengthToMany(self, node:FunctionCallNode):
        """ Return true if the function call has to many arguments """
        args = node.getArguments()
        if len(args) == 0:
            return False
        if args[0].exprtype != self.flagmap['s']:
            return False
        formatstring = args[0].content
        count = len(re.findall('\%[0-9]*[dfcs]', formatstring))
        return (count < (len(args) - 1))

    def getArgumentTypes(self):
        """ Return the type nodes of the parameters """
        types = []
        types.append(self.flagmap['s'])
        return types

    def checkFormatString(self, node:FunctionCallNode):
        """ Check if types match"""
        formatstring = node.getArguments()[0].content
        params = []
        for i in range(0, len(formatstring) - 2):
            if (formatstring[i] == '%') and (formatstring[i+1] in "dfcs"):
                if self.getName() == "scanf":
                    p = PointerTypeNode()
                    p.addChild(self.flagmap[formatstring[i+1]])
                    params.append(p)
                else: #printf
                    params.append(self.flagmap[formatstring[i+1]])
        args = node.getArguments()[1:]
        for i in range(0, len(params)):
            if args[i].exprtype != params[i]:
                return True
        return False
