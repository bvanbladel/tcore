"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from AST.TypeNode import TypeNode

class IntTypeNode(TypeNode):
    """ Node for int type
    """

    def __init__(self):
        """ Constructor """
        super().__init__()

    def __str__(self):
        """ String representation """
        return "int"


class CharTypeNode(TypeNode):
    """ Node for char type
    """

    def __init__(self):
        """ Constructor """
        super().__init__()

    def __str__(self):
        """ String representation """
        return "char"


class FloatTypeNode(TypeNode):
    """ Node for float type
    """

    def __init__(self):
        """ Constructor """
        super().__init__()

    def __str__(self):
        """ String representation """
        return "float"

class DoubleTypeNode(TypeNode):
    """ Node for double type
    """

    def __init__(self):
        """ Constructor """
        super().__init__()

    def __str__(self):
        """ String representation """
        return "double"


class VoidTypeNode(TypeNode):
    """ Node for void type
    """

    def __init__(self):
        """ Constructor """
        super().__init__()

    def __str__(self):
        """ String representation """
        return "void"


class BoolTypeNode(TypeNode):
    """ Node for bool type
    """

    def __init__(self):
        """ Constructor """
        super().__init__()

    def __str__(self):
        """ String representation """
        return "bool"
