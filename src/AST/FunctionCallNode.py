"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from AST.ExpressionNode import ExpressionNode
from AST.VariableNode import VariableNode

class FunctionCallNode(ExpressionNode):
    """ Node for a function call
    """

    def __init__(self):
        """ Constructor """
        super().__init__()
        self.declaration = None

    def getName(self):
        """ Return the function name"""
        return str(self.childNodes[0])

    def getArguments(self):
        """ Return the argument list """
        return self.childNodes[1:]

class FunctionMemberCallNode(FunctionCallNode):
    """ Node for a member function call
    """

    def __init__(self):
        """ Constructor """
        super().__init__()

    def getObject(self):
        """ Return the object being called """
        if isinstance(self.childNodes[0], VariableNode):
            return self.childNodes[0]
        else:
            return self.childNodes[0].getObject()

    def getName(self):
        """ Return the function name"""
        return str(self.childNodes[1])

    def getArguments(self):
        """ Return the argument list """
        return self.childNodes[2:]

    def isConstMethod(self):
        """ Return True if the method called was declared const """
        return self.declaration.isConst()
