"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from AST.ExpressionNode import ExpressionNode

class LiteralNode(ExpressionNode):
    """ Node for an literal
    """

    def __init__(self, content:str):
        """ Constructor """
        super().__init__()
        self.content = content

    def __str__(self):
        """ String representation """
        return self.content

    def __eq__(self, other):
        """ Equal """
        return isinstance(self, type(other)) and (self.childNodes == other.childNodes) and (self.content == other.content)

    def getValue(self):
        """ Return the value of the literal (same as content, but as a non-string)"""
        if self.content in ["True", "true"]:
            return 1
        elif self.content in ["False", "false"]:
            return 0
        elif self.content == "null":
            return None

        try:
            return int(self.content)
        except ValueError:
            try:
                return float(self.content)
            except:
                return None
