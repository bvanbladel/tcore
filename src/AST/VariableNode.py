"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from AST.ExpressionNode import ExpressionNode

class VariableNode(ExpressionNode):
    """ Node for an identifier
    """

    def __init__(self):
        """ Constructor """
        super().__init__()
        self.declaration = None
        self.lhs = False

    def getName(self):
        """ return the variable name"""
        if len(self.childNodes) == 1:
            return str(self.childNodes[0])
        else:
            return "%s.%s" % (str(self.childNodes[0].getName()), str(self.childNodes[-1]))
