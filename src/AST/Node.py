"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class Node:
    """ Interface for all nodes of the AST

    This class will be derived for each type of node in the AST,
    providing a shared interface.
    """

    def __init__(self):
        """ Constructor """
        self.parent     = None
        self.childNodes = []
        self.trace = {'lineNr': 0, 'charPos': 0, 'symbol': " "}

    def __str__(self):
        """ String representation """
        return self.__class__.__name__

    def __eq__(self, other):
        """ Equal """
        return isinstance(self, type(other)) and (self.childNodes == other.childNodes)

    def __ne__(self, other):
        """ NOT Equal """
        return not (self == other)

    def __hash__(self):
        """ Hash function """
        return hash(repr(self))

    def accept(self, visitor):
        """ Function that accepts the visitor"""
        visitor.visit(self)

    def getParent(self):
        """ Get the parent node"""
        return self.parent

    def hasChildNodes(self):
        """ Check if the node has children """
        return bool(len(self.childNodes))

    def getChildNodes(self):
        """ Get the child nodes"""
        return self.childNodes

    def addChild(self, child):
        """ Add a child node """
        self.childNodes.append(child)
        child.parent = self

    def containsVariable(self, variableNode):
        """ Verify whether this tree contains a variable (as lhs) """
        if (self == variableNode and self.getName() == variableNode.getName() and self.lhs == True):
            return True
        for child in self.getChildNodes():
            if child.containsVariable(variableNode):
                return True
        return False

    def findVariable(self, variableNode):
        """ Find the variable in this tree (as lhs) """
        if (self == variableNode and self.getName() == variableNode.getName() and self.lhs == True):
            return self
        for child in self.getChildNodes():
            n = child.findVariable(variableNode)
            if n != None:
                return n
        return None

    def replaceChild(self, currentChild, newChild):
        """ Replace a child with a new node """
        for i in range(0,len(self.childNodes),1):
            if self.childNodes[i] == currentChild:
                self.childNodes[i] = newChild
                newChild.parent = self
                return

    def getSubtreeSize(self):
        """ Returns the size of the subtree this node is root of """
        size = 1
        for i in range(0,len(self.childNodes),1):
            size += self.childNodes[i].getSubtreeSize()
        return size
