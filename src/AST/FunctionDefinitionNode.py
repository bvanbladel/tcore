"""
   CORE Testing Framework
   Copyright (C) 2018 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from AST.Node import Node

class FunctionDefinitionNode(Node):
    """ Node for a function definition
    """
    
    def __init__(self):
        """ Constructor """
        super().__init__()
        self.labelNr = 0
        self.localsNr = 0
        self.maxExprSize = 0
        
    def getDeclaration(self):
        """ Return the function declaration node """
        return self.childNodes[0]
        
    def getNrOfLocals(self):
        """ Return the number of locals used in the function body 
        (array counts as one local per element)"""
        return self.localsNr
        
    def getMaxExpressionSize(self):
        """ Return the maximim size of an expression used in the function body """
        return self.maxExprSize
