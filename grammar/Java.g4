/*
   CORE Testing Framework
   Copyright (C) 2017 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

grammar Java;

program
    : statements EOF
    ;

statements
    : statement statements
    | /* empty */
    ;

statement
    : javaImport SEMICOLON #importStatement
    | lineStmt SEMICOLON+  #lineStatement
    | blockStmt SEMICOLON? #blockStatement
    | functionDeclaration  #functionDeclarationStatement
    | classDeclaration     #classDeclarationStatement
    ;

lineStmt
    : variableDeclarationList  #declaration
    | expr                     #expression
    | THROW expr               #throwStatement
    | RETURN expr?             #returnStatement
    | BREAK                    #breakStatement
    | CONTINUE                 #continueStatement
    ;

blockStmt
    : IF ROUNDBRACKETOPEN expr ROUNDBRACKETCLOSE block                  #ifStatement
    | IF ROUNDBRACKETOPEN expr ROUNDBRACKETCLOSE block ELSE blockStmt   #ifElseStatement
    | WHILE ROUNDBRACKETOPEN expr ROUNDBRACKETCLOSE block               #whileStatement
    | DO block WHILE ROUNDBRACKETOPEN expr ROUNDBRACKETCLOSE            #doWhileStatement
    | FOR ROUNDBRACKETOPEN (expr|decl)? SEMICOLON
            (expr|decl)? SEMICOLON (expr|decl)? ROUNDBRACKETCLOSE block #forStatement
    | FOR ROUNDBRACKETOPEN typeName IDENTIFIER COLON
            expr ROUNDBRACKETCLOSE block                                #rangedForStatement
    | TRY block catchStatement+                                         #tryStatement
    | functionDefinition                                                #funcdef
    | classDefinition                                                   #classdef
    | block                                                             #compoundStatements
    ;

decl
    : variableDeclarationList
    | functionDeclaration
    | classDeclaration
    ;

variableDeclarationList
    : accessModifier? modifier* typeName variableDeclaration (COMMA variableDeclaration)*
    ;

variableDeclaration
    : IDENTIFIER
    | IDENTIFIER EQUAL expr
    | IDENTIFIER BRACKETOPEN BRACKETCLOSE EQUAL expr
    | IDENTIFIER BRACKETOPEN literal BRACKETCLOSE
    ;

functionDefinition
    : functionDeclaration block
    ;

functionDeclaration
    : accessModifier? modifier* (typeName)? IDENTIFIER ROUNDBRACKETOPEN parameters ROUNDBRACKETCLOSE (THROWS IDENTIFIER (COMMA IDENTIFIER)* )?
    ;

classDefinition
    : annotation? classDeclaration CURLYBRACKETOPEN members CURLYBRACKETCLOSE
    ;

classDeclaration
    : accessModifier? modifier* CLASS typeName (EXTENDS typeName (COMMA typeName)*)? (IMPLEMENTS typeName (COMMA typeName)*)?
    ;

parameters
    : /* empty */
    | parameter (COMMA parameter)*
    ;

parameter
    : modifier* typeName IDENTIFIER
    | modifier* typeName IDENTIFIER BRACKETOPEN expr? BRACKETCLOSE
    ;

catchStatement
    : CATCH ROUNDBRACKETOPEN (expr | decl) ROUNDBRACKETCLOSE block
    ;

expr
    : expr (PERIOD) expr                        #pipedExpression
    | expr arrayAccess                          #arrayAccessExpression
    | ROUNDBRACKETOPEN expr ROUNDBRACKETCLOSE   #nestedExpression
    | expr unaryOperator                        #unaryPostExpression
    | unaryOperator expr                        #unaryPreExpression
    | prefixUnaryOperator expr                  #prefixExpression
    | ROUNDBRACKETOPEN typeName ROUNDBRACKETCLOSE expr #castExpression
    | NEW expr                                  #newExpression
    | NEW expr CURLYBRACKETOPEN
        members CURLYBRACKETCLOSE               #inlineFunction
    | expr (ASTERISK|SLASH|PERCENT) expr        #binaryExpression
    | expr (PLUS|MINUS) expr                    #binaryExpression
    | expr (ANGLEBRACKETOPEN
        |ANGLEBRACKETCLOSE
        |ANGLEBRACKETOPENEQUAL
        |ANGLEBRACKETCLOSEEQUAL) expr           #binaryExpression
    | expr (EQUALEQUAL
        |EXCLAMATIONMARKEQUAL) expr             #binaryExpression
    | expr (AMPERSANDAMPERSAND) expr            #binaryExpression
    | expr (BARBAR) expr                        #binaryExpression
    | expr (INSTANCEOF) expr                  #binaryExpression
    | expr EQUAL expr                           #assignment
    | expr (PLUSEQUAL | MINUSEQUAL
        | ASTERISKEQUAL | SLASHEQUAL) expr      #assignmentExpression
    | expr QUESTIONMARK expr COLON expr         #ternaryExpression
    | typeName? arrayInitializer                #arrayInitializerExpression
    | literal                                   #literalExpression
    | variable                                  #identifierExpression
    | functionCall                              #functionCallExpression
    | typeName                                  #typeExpression
    | annotation                                #annotationExpression
    ;

arrayInitializer
    : CURLYBRACKETOPEN CURLYBRACKETCLOSE
    | CURLYBRACKETOPEN expr (COMMA expr)* CURLYBRACKETCLOSE
    ;

variable
    : IDENTIFIER
    | typeName PERIOD CLASS
    ;

unaryOperator
    : PLUSPLUS
    | MINUSMINUS
    ;

prefixUnaryOperator
    : EXCLAMATIONMARK
    | MINUS
    | PLUS
    ;

arrayAccess
    : BRACKETOPEN expr BRACKETCLOSE
    ;

functionCall
    : typeName ROUNDBRACKETOPEN ROUNDBRACKETCLOSE
    | typeName ROUNDBRACKETOPEN arguments ROUNDBRACKETCLOSE
    ;

arguments
    : expr
    | expr COMMA arguments
    ;

block
    : CURLYBRACKETOPEN (lineStmt SEMICOLON+ | blockStmt SEMICOLON?)* CURLYBRACKETCLOSE
    | lineStmt
    ;

members
    : member members
    | /* empty */
    ;

member
    : accessModifier? modifier* variableDeclarationList SEMICOLON?
    | annotation* functionDefinition SEMICOLON?
    | annotation* functionDeclaration SEMICOLON?
    | classDefinition
    | STATIC block SEMICOLON?
    ;

annotation
    : AT IDENTIFIER (ROUNDBRACKETOPEN .*? ROUNDBRACKETCLOSE)?
    ;

accessModifier
    : PUBLIC
    | PROTECTED
    | PRIVATE
    ;

modifier
    : STATIC
    | FINAL
    | ABSTRACT
    | ANGLEBRACKETOPEN typeName (COMMA typeName)* ANGLEBRACKETCLOSE
    ;

typeName
    : INT
    | CHAR
    | FLOAT
    | DOUBLE
    | VOID
    | BOOLEAN
    | IDENTIFIER
    | typeName BRACKETOPEN BRACKETCLOSE
    | typeName ANGLEBRACKETOPEN typeName (COMMA typeName)* ANGLEBRACKETCLOSE
    | typeName PERIOD typeName
    | typeName EXTENDS typeName
    | QUESTIONMARK
    ;

javaImport
    : (IMPORT | PACKAGE) STATIC? (IDENTIFIER | ASTERISK | PERIOD)+
    ;


// keywords
CLASS:                      'class';
PUBLIC:                     'public';
PROTECTED:                  'protected';
PRIVATE:                    'private';
STATIC:                     'static';
FINAL:                      'final';
ABSTRACT:                   'abstract';
WHILE:                      'while';
DO:                         'do';
IF:                         'if';
ELSE:                       'else';
RETURN:                     'return';
FOR:                        'for';
BREAK:                      'break';
CONTINUE:                   'continue';
INT:                        'int';
CHAR:                       'char';
FLOAT:                      'float';
DOUBLE:                     'double';
VOID:                       'void';
BOOLEAN:                    'boolean';
CONST:                      'const';
NEW:                        'new';
IMPORT:                     'import';
PACKAGE:                    'package';
THROW:                      'throw';
THROWS:                     'throws';
EXTENDS:                    'extends';
IMPLEMENTS:                 'implements';
TRUE:                       'true';
FALSE:                      'false';
NULL:                       'null';
TRY:                        'try';
CATCH:                      'catch';
INSTANCEOF:                 'instanceof';

// compound punctuation
PLUSPLUS:                   '++';
MINUSMINUS:                 '--';
EQUALEQUAL:                 '==';
ANGLEBRACKETOPENEQUAL:      '<=';
ANGLEBRACKETCLOSEEQUAL:     '>=';
AMPERSANDAMPERSAND:         '&&';
BARBAR:                     '||';
EXCLAMATIONMARKEQUAL:       '!=';
PLUSEQUAL:                  '+=';
MINUSEQUAL:                 '-=';
ASTERISKEQUAL:              '*=';
SLASHEQUAL:                 '/=';

// brackets
BRACKETOPEN:                '[';
BRACKETCLOSE:               ']';
ROUNDBRACKETOPEN:           '(';
ROUNDBRACKETCLOSE:          ')';
CURLYBRACKETOPEN:           '{';
CURLYBRACKETCLOSE:          '}';
ANGLEBRACKETOPEN:           '<';
ANGLEBRACKETCLOSE:          '>';

// punctuation
EXCLAMATIONMARK:            '!';
COMMA:                      ',';
PERIOD:                     '.';
COLON:                      ':';
SEMICOLON:                  ';';
EQUAL:                      '=';
PLUS:                       '+';
MINUS:                      '-';
ASTERISK:                   '*';
SLASH:                      '/';
PERCENT:                    '%';
AMPERSAND:                  '&';
AT:                         '@';
BAR:                        '|';
SINGLEQUOTE:                '\'';
DOUBLEQUOTE:                '"';
QUESTIONMARK:               '?';

literal
    : NUMERIC
    | ALPHANUMERIC
    | TRUE
    | FALSE
    | NULL
    ;

NUMERIC
    : [0-9]+ ('L' | 'l' | 'f' | 'd')?
    | [0-9]+ PERIOD [0-9]* ('L' | 'l' | 'f' | 'd')?
    | PERIOD [0-9]+ ('L' | 'l' | 'f' | 'd')?
    | [0-9]+ (PERIOD [0-9]*)? ('e'|'E') MINUS? [0-9]+ (PERIOD [0-9]*)?
    | '0x' [0-9a-f]* ('L' | 'l' | 'f' | 'd')?
    ;

ALPHANUMERIC
    : SINGLEQUOTE ~['\r\n] SINGLEQUOTE
    | DOUBLEQUOTE ~["\r\n]* DOUBLEQUOTE
    ;

IDENTIFIER
    : [a-zA-Z][a-zA-Z0-9_]*
    ;

SingleLineComment : '//' ~[\r\n]* -> skip;
MultiLineComment : '/*' .*? '*/' -> skip;
WhiteSpace : [ \r\t\n]+ -> skip ;
