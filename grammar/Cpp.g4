/*
   CORE Testing Framework
   Copyright (C) 2017 Brent van Bladel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

grammar Cpp;

program
    : statements
    ;

statements
    : statement statements
    | /* empty */
    ;

statement
    : INCLUDE              #includeStatement
    | NAMESPACE SEMICOLON  #namespaceStatement
    | lineStmt SEMICOLON   #lineStatement
    | THROW expr SEMICOLON #throwStatement
    | blockStmt SEMICOLON? #blockStatement
    ;

lineStmt
    : decl             #declaration
    | expr             #expression
    | assign           #assignment
    | RETURN expr?     #returnStatement
    | BREAK            #breakStatement
    | CONTINUE         #continueStatement
    ;

blockStmt
    : IF ROUNDBRACKETOPEN expr ROUNDBRACKETCLOSE statement elsePart?                        #ifStatement
    | WHILE ROUNDBRACKETOPEN expr ROUNDBRACKETCLOSE (block | lineStmt)                      #whileStatement
    | FOR ROUNDBRACKETOPEN (expr|assign|decl)? SEMICOLON (expr|assign|decl)? SEMICOLON (expr|assign|decl)? ROUNDBRACKETCLOSE (block | lineStmt) #forStatement
    | FOR ROUNDBRACKETOPEN (typeName (ASTERISK | AMPERSAND)? IDENTIFIER COLON IDENTIFIER) ROUNDBRACKETCLOSE (block | lineStmt) #forStatement
    | TRY block CATCH ROUNDBRACKETOPEN expr ROUNDBRACKETCLOSE block                         #tryStatement
    | functionDefinition                                                                    #funcdef
    | classDeclaration CURLYBRACKETOPEN classContent* CURLYBRACKETCLOSE                     #classDefinition
    | block                                                                                 #compoundStatements
    ;

elsePart
    : ELSE IF ROUNDBRACKETOPEN expr ROUNDBRACKETCLOSE statement elsePart  #elseIfStatement
    | ELSE statement                                                      #elseStatement
    ;

decl
    : variableDeclaration
    | functionDeclaration
    | classDeclaration
    ;

variableDeclaration
    : typeName ASTERISK* IDENTIFIER
    | typeName ASTERISK* IDENTIFIER EQUAL expr
    | typeName ASTERISK* IDENTIFIER BRACKETOPEN literal BRACKETCLOSE
    | typeName ASTERISK* IDENTIFIER listInitializer
    ;

functionDefinition
    : functionDeclaration initializerList? block
    ;

functionDeclaration
    : (CONST? typeName (ASTERISK* | AMPERSAND))? (IDENTIFIER COLONCOLON)? IDENTIFIER ROUNDBRACKETOPEN parameters ROUNDBRACKETCLOSE CONST?
    ;

classDeclaration
    : CLASS IDENTIFIER
    ;

parameters
    : /* empty */
    | parameter (COMMA parameter)*
    ;

parameter
    : CONST? typeName (ASTERISK* | AMPERSAND) IDENTIFIER (EQUAL expr)?
    | CONST? typeName (ASTERISK* | AMPERSAND) IDENTIFIER BRACKETOPEN expr BRACKETCLOSE
    ;

initializerList
    : COLON initializer (COMMA initializer)*
    ;

initializer
    : IDENTIFIER ROUNDBRACKETOPEN IDENTIFIER ROUNDBRACKETCLOSE
    ;

expr
    : literal                                   #literalExpression
    | (IDENTIFIER COLONCOLON)? IDENTIFIER       #identifierExpression
    | arrayAccess                               #arrayAccessExpression
    | functionCall                              #functionCallExpression
    | ROUNDBRACKETOPEN expr ROUNDBRACKETCLOSE   #nestedExpression
    | expr unaryOperator                        #unaryPostExpression
    | expr (PERIOD | ARROW) expr                #memberAccessExpression
    | unaryOperator expr                        #unaryPreExpression
    | prefixUnaryOperator expr                  #prefixExpression
    | expr (ASTERISK|SLASH|PERCENT) expr        #binaryExpression
    | expr (PLUS|MINUS) expr                    #binaryExpression
    | expr (SHIFTLEFT | SHIFTRIGHT) expr       #binaryExpression
    | expr (ANGLEBRACKETOPEN
        |ANGLEBRACKETCLOSE
        |ANGLEBRACKETOPENEQUAL
        |ANGLEBRACKETCLOSEEQUAL) expr           #binaryExpression
    | expr (EQUALEQUAL
        |EXCLAMATIONMARKEQUAL) expr             #binaryExpression
    | expr (AMPERSANDAMPERSAND) expr            #binaryExpression
    | expr (BARBAR) expr                        #binaryExpression
    | listInitializer                           #listInitializerExpression
    ;

listInitializer
    : CURLYBRACKETOPEN arguments? CURLYBRACKETCLOSE
    ;

unaryOperator
    : PLUSPLUS
    | MINUSMINUS
    ;

prefixUnaryOperator
    : EXCLAMATIONMARK
    | MINUS
    | ASTERISK
    | AMPERSAND
    ;

arrayAccess
    : IDENTIFIER BRACKETOPEN expr BRACKETCLOSE
    ;

functionCall
    : IDENTIFIER ROUNDBRACKETOPEN ROUNDBRACKETCLOSE
    | IDENTIFIER ROUNDBRACKETOPEN arguments ROUNDBRACKETCLOSE
    ;

arguments
    : expr
    | expr COMMA arguments
    ;

assign
    : ASTERISK? (IDENTIFIER COLONCOLON)? IDENTIFIER (EQUAL | PLUSEQUAL | MINUSEQUAL) expr     #variableAssign
    | ASTERISK? (IDENTIFIER COLONCOLON)? arrayAccess (EQUAL | PLUSEQUAL | MINUSEQUAL) expr    #arrayAssign
    ;

block
    : CURLYBRACKETOPEN statements CURLYBRACKETCLOSE
    ;

classContent
    : PUBLIC COLON members*       #publicAccess
    | PROTECTED COLON members*    #protectedAccess
    | PRIVATE COLON members*      #privateAccess
    | members+                    #privateAccess
    ;

members
    : variableDeclaration SEMICOLON
    | functionDeclaration SEMICOLON
    | functionDefinition SEMICOLON
    ;

typeName
    : INT
    | CHAR
    | FLOAT
    | VOID
    | AUTO
    | IDENTIFIER
    ;


// keywords
CLASS:                      'class';
PUBLIC:                     'public';
PROTECTED:                  'protected';
PRIVATE:                    'private';
WHILE:                      'while';
IF:                         'if';
ELSE:                       'else';
RETURN:                     'return';
FOR:                        'for';
BREAK:                      'break';
CONTINUE:                   'continue';
INT:                        'int';
CHAR:                       'char';
FLOAT:                      'float';
VOID:                       'void';
AUTO:                       'auto';
CONST:                      'const';
THROW:                      'throw';
TRY:                        'try';
CATCH:                      'catch';

// compound punctuation
PLUSPLUS:                   '++';
MINUSMINUS:                 '--';
EQUALEQUAL:                 '==';
ANGLEBRACKETOPENEQUAL:      '<=';
ANGLEBRACKETCLOSEEQUAL:     '>=';
AMPERSANDAMPERSAND:         '&&';
BARBAR:                     '||';
EXCLAMATIONMARKEQUAL:       '!=';
PLUSEQUAL:                  '+=';
MINUSEQUAL:                 '-=';
COLONCOLON:                 '::';
ARROW:                      '->';
SHIFTLEFT:                  '<<';
SHIFTRIGHT:                 '>>';


// brackets
BRACKETOPEN:                '[';
BRACKETCLOSE:               ']';
ROUNDBRACKETOPEN:           '(';
ROUNDBRACKETCLOSE:          ')';
CURLYBRACKETOPEN:           '{';
CURLYBRACKETCLOSE:          '}';
ANGLEBRACKETOPEN:           '<';
ANGLEBRACKETCLOSE:          '>';

// punctuation
EXCLAMATIONMARK:            '!';
COMMA:                      ',';
PERIOD:                     '.';
COLON:                      ':';
SEMICOLON:                  ';';
EQUAL:                      '=';
PLUS:                       '+';
MINUS:                      '-';
ASTERISK:                   '*';
SLASH:                      '/';
PERCENT:                    '%';
AMPERSAND:                  '&';
BAR:                        '|';
SINGLEQUOTE:                '\'';
DOUBLEQUOTE:                '"';


literal
    : NUMERIC
    | ALPHANUMERIC
    ;

NUMERIC
    : [0-9]+ 'f'?
    | [0-9]+ PERIOD [0-9]* 'f'?
    | PERIOD [0-9]+ 'f'?
    ;

ALPHANUMERIC
    : SINGLEQUOTE ~['\r\n]* SINGLEQUOTE
    | DOUBLEQUOTE ~["\r\n]* DOUBLEQUOTE
    ;

IDENTIFIER
    : [a-zA-Z][a-zA-Z0-9_]*
    ;

INCLUDE
    : '#include' ' '* '<' [0-9a-zA-Z.]* '>'
    | '#include' ' '* '"' [0-9a-zA-Z.]* '"'
    | '#ifndef' ' '* [0-9a-zA-Z._]*
    | '#define' ' '* [0-9a-zA-Z._]*
    | '#endif'
    ;

NAMESPACE
    : 'using' ' '* 'namespace' ' '* [a-zA-Z]*
    ;


SingleLineComment : '//' ~[\r\n]* -> skip;
MultiLineComment : '/*' .*? '*/' -> skip;
WhiteSpace : [ \r\t\n]+ -> skip ;
